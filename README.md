TRIVIAL SPACE PLAYGROUND
========================

This is the playground for creating visualizations for [trivial space](http://trivialspace.net). It contains a minimal local server environment, and the source code of all libraries, utilities and the framework used to develop the visualizations.


Installation
------------

You need to have [node.js](http://nodejs.org) installed to start the server. Get it first before continuing the installation.

clone this repository into a folder of your choise by executing the command

    git clone https://github.com/trivial-space/local-dev-env.git

in the your terminal or console.

Then to run the server, run

    cd local-dev-env
    npm install
    node app

Then open the url `localhost:8080/` in your browser to see all available drafts in action.

You can explore the source code of all the drafts in the `public/drafts/` folder. Each draft contains an own README.md file which provides more information about the specific draft.

Enjoy playing!


License
-------

Unless otherwise specified in the source files, all code is released under the MIT-License. See MIT-LICENSE.txt for the full License text.

All artworks and assets like textures, sounds and 3D-Models are licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License. See CC-BY-SA-LICENSE.txt for the full text. This hold unless explicitly specified otherwise in the README.md files of specific drafts.

