express = require 'express'
assets = require 'connect-assets'
engines = require 'consolidate'
draftcontroller = require './server/draftcontroller'

PUBLIC_PATH = __dirname + "/public"
SERVER_PORT = 8080

app = express()

app.set 'view engine', 'eco'
app.engine 'eco', engines.eco
app.set 'views', 'server/views'
app.use app.router
app.use assets src: PUBLIC_PATH, buildDir: false
app.use express.static PUBLIC_PATH
app.use express.errorHandler dumpExceptions: true, showStack: true

# needed to provide compiled coffeescript files
app.get /^(\/[^\s]+)\.js$/, (req, res, next) ->
  js req.params[0]
  next()

# needed to provide compiled less files
app.get /^(\/[^\s]+)\.css$/, (req, res, next) ->
  css req.params[0]
  next()

draftcontroller.init app

module.exports = app

app.listen SERVER_PORT
console.log "server started on port " + SERVER_PORT
