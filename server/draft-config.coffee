module.exports =

  metalplates:
    path: "metalplates"
    appName: "MetalPlatesApp"

  tworooms:
    path: "tworooms"
    appName: "TwoRoomsApp"

  worms:
    path: "worms"
    appName: "WormsApp"

  canvastests:
    path: "canvastests"
    appName: "CanvasTests"

  "raw shader":
    path: "rawshader"
    appName: "RawShaderApp"

  "nano fuzz":
    path: "nanofuzz"
    appName: "NanoFuzzApp"

  "behind glass":
    path: "behindglass"
    appName: "BehindGlassApp"

  "three screens":
    path: "threescreens"
    appName: "ThreeScreensApp"

  "balloon":
    path: "balloon"
    appName: "BalloonApp"

