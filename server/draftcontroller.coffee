drafts = require "./draft-config"

title = "trivial space - "

renderIndex = (res, draftname) ->
  draft = drafts[draftname]
  draft.name = draftname
  res.render 'index',
    title: title + draftname
    draft: draft
    drafts: drafts

exports.init = (app) ->

  app.get "/tests", (req, res, next) ->
    res.render 'tests',
      title: title + "unit-tests"

  app.get "/:draftname", (req, res, next) ->
    draftname = req.params.draftname
    if drafts[draftname]
      renderIndex res, draftname
    else next()

  app.get "/", (req, res) ->
    draftname = (name for name of drafts)[0]
    renderIndex res, draftname
