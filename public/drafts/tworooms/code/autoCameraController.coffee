define [

  "lib/utils/math/functions"
  "lib/utils/math/noise"

], ({sign}
    {noise1D}) ->


  offsetLookX = 23.45566
  offsetLookY = 5.123456
  offsetSpeed = 3.544332
  lookTarget = new THREE.Vector3()
  walkAngle = 0
  walkRadius = 26
  currentStep = 1

  lookAt = (camera, angleX, angleY) ->
    angleX = Math.PI / 2 - angleX
    sinX = Math.sin angleX
    position = camera.position
    lookTarget.x = position.x + 100 * sinX * Math.cos angleY
    lookTarget.y = position.y + 100 * Math.cos angleX
    lookTarget.z = position.z + 100 * sinX * Math.sin angleY
    camera.lookAt lookTarget


  noisePow2 = (x) ->
    noise = noise1D x
    noise * noise * sign noise


  updateCamera = (camera, step) ->
    currentStep += step
    stepLookX = (currentStep + offsetLookX) * 0.15
    stepLookY = (currentStep + offsetLookY) * 0.25
    noiseLookX1 = noisePow2 stepLookX
    noiseLookX2 = noise1D stepLookX * 2
    noiseLookY1 = noisePow2 stepLookY
    noiseLookY2 = noise1D stepLookY * 2
    noiseMove = noise1D currentStep * 0.05
    stepSpeed = (currentStep + offsetSpeed) * 0.2
    noiseSpeed = (noise1D stepSpeed) + 0.3
    
    walkAngle += step * 0.3 # * noiseSpeed
    sinWalk = Math.sin walkAngle
    cosWalk = Math.cos walkAngle + 0.8
    camera.position.x = (sinWalk + 1.0) * walkRadius + 5
    camera.position.z = - (sinWalk * sinWalk + 1.0) * walkRadius * 0.7 + 13 + noiseMove

    lookAngleX = noiseLookX1 * Math.PI * 0.2 + noiseLookX2 * Math.PI * 0.1 + 0.03 * Math.PI
    lookAngleY = noiseLookY1 * Math.PI * 0.4 + noiseLookY2 * Math.PI * 0.15
    lookAngleY -= Math.PI / 2 - cosWalk * Math.PI / 1.7
    lookAt camera, lookAngleX, lookAngleY 


  exports = 
    updateCamera: updateCamera