uniform float opacity;
uniform sampler2D tDiffuse;
uniform sampler2D original;

varying vec2 vUv;

void main() {

    vec4 blur = texture2D( tDiffuse, vUv );
    vec4 texel = texture2D( original, vUv );
    float strength = blur.r * blur.g * blur.b;
    
    vec3 hsl = RGBToHSL(blur.rgb);
    hsl.y = mix(hsl.y, min(1.0, hsl.y * 3.0), strength * strength);
    blur.rgb = HSLToRGB(hsl);

    strength = sqrt(sqrt(strength));
    strength *= opacity;

    vec3 color = max(texel.rgb, mix(texel.rgb, blur.rgb, strength));
    color = color + blur.rgb * strength * 0.08;

    gl_FragColor = vec4(color, 1.0);
}
