varying vec2 vUv;
varying vec4 vPosition;

uniform sampler2D tDiffuse;
uniform sampler2D tReflection;

void main(){
    vec4 projCoord = vPosition / vPosition.w;
    projCoord =(projCoord+1.0)*0.5;
    projCoord = clamp(projCoord, 0.001, 0.999);
    
    vec4 texCol = texture2D(tDiffuse, vUv);
    vec4 reflCol = texture2D(tReflection, vec2(projCoord.x,1.0-projCoord.y));

    float light= clamp((reflCol.x+reflCol.y+reflCol.z)/(3.0),0.0,1.0);
    light = light - 0.3;
    light = clamp(light,0.13,0.4);

    gl_FragColor = mix(texCol, reflCol, light);
    //gl_FragColor = mix(texCol, reflCol, 0.2);
    /* gl_FragColor = reflCol; */
}
