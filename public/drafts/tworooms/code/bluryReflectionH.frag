uniform sampler2D tDiffuse; 
uniform sampler2D tHeight;
varying vec2 vUv;



void main(void)
{
    float height = texture2D(tHeight,vUv).x;
    float blurSize = height/120.0;
    vec4 sum = vec4(0.0);

    // blur in y (horizontal)
    // take nine samples, with the distance blurSize between them
    /*sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y - 4.0*blurSize)) * 0.05;
    sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y - 3.0*blurSize)) * 0.09;
    sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y - 2.0*blurSize)) * 0.12;
    sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y - blurSize)) * 0.15;
    sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y)) * 0.16;
    sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y + blurSize)) * 0.15;
    sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y + 2.0*blurSize)) * 0.12;
    sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y + 3.0*blurSize)) * 0.09;
    sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y + 4.0*blurSize)) * 0.05;
    */

    sum += texture2D(tDiffuse, vec2(vUv.x - 4.0*blurSize, vUv.y)) * 0.11;
    sum += texture2D(tDiffuse, vec2(vUv.x - 3.0*blurSize, vUv.y)) * 0.11;
    sum += texture2D(tDiffuse, vec2(vUv.x - 2.0*blurSize, vUv.y)) * 0.11;
    sum += texture2D(tDiffuse, vec2(vUv.x - blurSize, vUv.y)) * 0.11;
    sum += texture2D(tDiffuse, vec2(vUv.x, vUv.y)) * 0.12;
    sum += texture2D(tDiffuse, vec2(vUv.x + blurSize, vUv.y)) * 0.11;
    sum += texture2D(tDiffuse, vec2(vUv.x + 2.0*blurSize, vUv.y)) * 0.11;
    sum += texture2D(tDiffuse, vec2(vUv.x + 3.0*blurSize, vUv.y)) * 0.11;
    sum += texture2D(tDiffuse, vec2(vUv.x + 4.0*blurSize, vUv.y)) * 0.11;


    gl_FragColor = sum;
}
