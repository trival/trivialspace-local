define [
  "text!./bluryReflectionH.frag"
  "js!external/threejs_extras/shaders/CopyShader"
], (fragmentShader) ->

  exports =
    vertexShader: THREE.CopyShader.vertexShader
    fragmentShader: fragmentShader
    uniforms:
      "tHeight":
        type: 't'
        value: null
      "tDiffuse":
        type: 't'
        value: null
