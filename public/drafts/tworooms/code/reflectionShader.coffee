define [
  "text!./reflection.vert"
  "text!./reflection.frag"
], (vertexShader, fragmentShader) ->

  exports =
    vertexShader: vertexShader
    fragmentShader: fragmentShader
    uniforms:
      "tReflection":
        type: 't'
        value: null
      "tDiffuse":
        type: 't'
        value: null
