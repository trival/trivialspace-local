varying float height;
void main() {
    gl_FragColor = vec4(vec3(height), 1.0);
}
