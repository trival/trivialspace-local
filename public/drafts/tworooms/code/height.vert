varying float height;
void main() {
    vec4 world = modelMatrix * vec4( position, 1.0 );
    height = clamp(world.y / 10.0, 0.0, 1.0);
    gl_Position = projectionMatrix * viewMatrix * world;
}
