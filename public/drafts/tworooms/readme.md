__Two rooms__ is the first implementation of a possible virtual gallery showroom. It tests some lighting and illumination ideas for an realistic art space experience. The main challenge is to reconstruct the elaborate indirect lighting situation which can be found in galeries and museums in an believable and authentic way.

Code, artwork and water color texture by Thomas Gorny.

## Navigation

* Hold __left mouse button__ and __drag__ mouse to look
* Use __arrows__ or __'wasd'__ or __right mouse button__ to move

## Planned updates

* rebuild the ceiling model for a more realistic impression of the indirect lighting effect
* implement collision detection with the walls ; )
* rethink the reflecting algorithm
* more light tuning

## Changelog

__version 0.2__ (2012-10-08): 

* First implementation of reflecting ground
* Tuned up light bloom effect on highlighted parts

__version 0.1__ (2012-04-13): 

* Initial release
