### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [
  'lib/app/BasicThreeJsApp'
  'lib/app/FirstPersonControls'
  'draft/code/BloomPass'
  'draft/code/autoCameraController'

  'lib/utils/threejs/helpers'

  'text!draft/models/2rooms-walls.js'
  'text!draft/models/2rooms-roof.js'
  'text!draft/models/2rooms-bottom.js'
  'text!draft/models/2rooms-door.js'

  "text!draft/code/height.vert"
  "text!draft/code/height.frag"

  "draft/code/reflectionShader"
  "draft/code/bluryReflectionV"
  "draft/code/bluryReflectionH"

  'js!external/threejs_extras/shaders/FXAAShader'
  'js!external/threejs_extras/postprocessing/ShaderPass'
  'js!external/threejs_extras/postprocessing/MaskPass'
  'js!external/threejs_extras/postprocessing/BloomPass'
  'js!external/threejs_extras/postprocessing/RenderPass'
  'js!external/threejs_extras/postprocessing/EffectComposer'

], (BasicApp, FirstPersonControls, BloomPass,
    {updateCamera}
    {renderTarget}
    wallsModel, roofModel, bottomModel, doorModel,
    heightVert, heightFrag,
    reflectionShader, bluryReflectionV, bluryReflectionH) ->


  # small bugfix until new THREEJS version
  THREE.Vector3::getDirectionFromMatrix = (matrix) ->
    me = matrix.elements;
    @x = me[8];
    @y = me[9];
    @z = me[10];
    return this;


  class TwoRoomsApp extends BasicApp

    mesh: null
    controls: null
    composer: null
    cameraBottom: null

    textureUrls: [
      PATH_TO_DRAFT + "textures/walls.jpg"
      PATH_TO_DRAFT + "textures/beton.png"
    ]


    init: () ->

      @directRender = false
      @renderer.setClearColorHex 0, 1
      @renderer.autoClear = false

      @camera.position.set 44, 5, -15
      @camera.lookAt new THREE.Vector3 46, 5, -15

      @lookDir = new THREE.Vector3

      @cameraBottom = new THREE.PerspectiveCamera 60, @width / @height, 0.01, 100
      @scene.add @cameraBottom
      @cameraBottom.position.set 44, -5, -15
      @cameraBottom.lookAt new THREE.Vector3 46, -5, -15

      @bottomDepth = renderTarget @width, @height
      @bottomReflection = renderTarget @width, @height

      @materialHeight = new THREE.ShaderMaterial
        vertexShader: heightVert
        fragmentShader: heightFrag

      @composerReflection = new THREE.EffectComposer @renderer, @bottomReflection
      @composerReflection.addPass new THREE.RenderPass @scene, @cameraBottom
      bluryReflectionH.uniforms.tHeight.value = @bottomDepth
      bluryReflectionV.uniforms.tHeight.value = @bottomDepth
      @composerReflection.addPass new THREE.ShaderPass bluryReflectionV
      @composerReflection.addPass new THREE.ShaderPass bluryReflectionH
      @composerReflection.addPass new THREE.ShaderPass THREE.CopyShader

      @controls = new FirstPersonControls(@camera, @domElement)
      @controls.manipulateMoveTarget = @manipulateMoveTarget
      @controls.movementSpeed = 1.7

      [wallTex, betonTex] = @textures
      wallsMat = new THREE.MeshBasicMaterial map: wallTex
      betonMat = new THREE.MeshBasicMaterial map: betonTex
      reflectionShader.uniforms.tReflection.value = @bottomReflection
      reflectionShader.uniforms.tDiffuse.value = betonTex
      bottomMat = new THREE.ShaderMaterial reflectionShader

      walls = new THREE.Mesh @modelLoader.load(wallsModel), wallsMat
      door = new THREE.Mesh @modelLoader.load(doorModel), betonMat
      bottom = new THREE.Mesh @modelLoader.load(bottomModel), bottomMat
      roof = new THREE.Mesh @modelLoader.load(roofModel), betonMat

      @scene.add walls
      @scene.add door
      @scene.add bottom
      @scene.add roof

      renderModel = new THREE.RenderPass @scene, @camera
      @effectBloom = new BloomPass @width, @height, 1, 25, 5.5
      @effectFXAA = new THREE.ShaderPass THREE.FXAAShader
      @effectFXAA.uniforms["resolution"].value.set 1 / @width, 1 / @height
      @effectFXAA.renderToScreen = true

      @composer = new THREE.EffectComposer @renderer
      @composer.addPass renderModel
      @composer.addPass @effectBloom
      @composer.addPass @effectFXAA


    update: ->
      # @controls.update()
      updateCamera @camera, 0.002
      @updateReflection()
      @composer.render()


    updateReflection: ->
      position = @camera.position.clone()
      position.y = -position.y
      @cameraBottom.position = position
      @lookDir.getDirectionFromMatrix @camera.matrix
      @lookDir.z = -@lookDir.z
      @lookDir.x = -@lookDir.x
      @cameraBottom.lookAt @lookDir.add @cameraBottom.position
      @scene.overrideMaterial = @materialHeight
      @renderer.render @scene, @cameraBottom, @bottomDepth, true
      @scene.overrideMaterial = null
      @composerReflection.render()
      @composerReflection.swapBuffers()


    manipulateMoveTarget: (target) =>
      target.y = 0


    onResize: (event) =>
      super event
      @effectBloom.resize @width, @height
      @effectFXAA.uniforms["resolution"].value = new THREE.Vector2 1 / @width, 1 / @height
