{

    "metadata" :
    {
        "formatVersion" : 3.1,
        "generatedBy"   : "Blender 2.63 Exporter",
        "vertices"      : 8,
        "faces"         : 4,
        "normals"       : 2,
        "colors"        : 0,
        "uvs"           : [8],
        "materials"     : 0,
        "morphTargets"  : 0,
        "bones"         : 0
    },

    "scale" : 1.000000,

    "materials": [],

    "vertices": [29,17,-29,29,17,-1,1.00001,17,-1,1.00001,17,-29,33,17,-29,33,17,-1,61,17,-1,61,17,-29],

    "morphTargets": [],

    "normals": [0,-1,0,0,-0.999969,0],

    "colors": [],

    "uvs": [[0.536788,0.999027,0.075451,0.999027,0.536788,0.53769,0.075451,0.53769,0.998923,0.537731,0.998923,0.999068,0.537586,0.537731,0.537586,0.999068]],

    "faces": [40,5,4,6,0,1,2,0,1,0,40,4,7,6,1,3,2,1,0,0,40,0,1,3,4,5,6,0,0,0,40,1,2,3,5,7,6,0,0,0],

    "bones" : [],

    "skinIndices" : [],

    "skinWeights" : [],

    "animation" : {}


}
