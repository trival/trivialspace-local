uniform sampler2D tNoise;
uniform sampler2D tLights;
uniform float time;

varying vec2 vUv;
varying float vHeight;
varying vec3 vQubeMapUv;

const float PI = 3.14159265359;
const float STRIPECOUNT = 110.0;

float calculateSurface(vec2 uv) {
	vec4 noise = texture2D(tNoise, uv * 6.0);
	float stripeVal = (uv.x + noise.x * 0.002) * STRIPECOUNT;
	float stripe = floor(stripeVal);
	float stripeCoord = fract(stripeVal);
	vec4 stripeNoise = texture2D(tNoise, vec2(stripe * 51.0 / STRIPECOUNT, uv.y * 17.0));
	float luminance = 0.95 + stripeNoise.z * 0.05;
	if (stripeCoord > 0.08 + (noise.w - 0.5) * 0.02) luminance -= 0.025;
	return luminance;
}

float circle(vec2 pos, float time, float y, vec4 noise){
	float noiseTime = time * (noise.x - 0.5);
	vec4 noiseX = texture2D(tNoise, vUv);
	float wave = sin(pos.x * PI + noiseTime + noise.z * 51.0) * pow(noise.x, 3.0) * y * (1.0 - noiseX.x * 0.3);
	wave = wave - pos.y - noise.y * sin(noiseTime * 0.5 + noise.y * 37.0);
	return pow(abs(wave) * (0.5 + y * 0.5), 0.1) * sign(wave);
}

vec4 getBackgroundLight() {
	vec3 uv = normalize(vQubeMapUv);
	float maxVal = max(abs(uv.x), max(abs(uv.y), abs(uv.z)));
	uv = uv / maxVal;
	vec2 lightUv = vec2(0.0);
	if (abs(uv.x) > 0.999) {
		if (abs(uv.y) > 0.99 || abs(uv.z) > 0.99) {
			return vec4(0.0);
		}
		lightUv = (uv.yz * 0.5 + 0.5) / 3.0;
		if (uv.x < 0.0) {
			lightUv.y += 1.0/ 3.0;
		}
		return texture2D(tLights, lightUv);
	} else if (abs(uv.y) > 0.999) {
		if (abs(uv.x) > 0.99 || abs(uv.z) > 0.99) {
			return vec4(0.0);
		}
		lightUv = (uv.xz * 0.5 + 0.5) / 3.0;
		lightUv.x += 1.0 / 3.0;
		if (uv.y < 0.0) {
			lightUv.y += 1.0/ 3.0;
		}
		return texture2D(tLights, lightUv);
	} else if (abs(uv.z) > 0.999) {
		if (abs(uv.y) > 0.99 || abs(uv.x) > 0.99) {
			return vec4(0.0);
		}
		lightUv = (uv.xy * 0.5 + 0.5) / 3.0;
		lightUv.x += 2.0 / 3.0;
		if (uv.z < 0.0) {
			lightUv.y += 1.0/ 3.0;
		}
		return texture2D(tLights, lightUv);
	}
	return vec4(0.0);
}


void main(void) {

	vec2 coord = vUv * 2.0 - 1.0;
	float y = cos(coord.y * PI * 0.5);
	float roaps = 1.0;
	float normal = 0.5;

	for (int i = 1; i < 20; i++) {
		float index = float(i) / 20.0;
		vec4 noise = texture2D(tNoise, vec2(index, index * 1.3));
		float sinus = circle(coord, time, y, noise);

		float newNormal = sinus * 0.5;
		if (newNormal < 0.0) {
			newNormal += 1.0;
		}
		normal = mix(newNormal, normal, abs(sinus) + index * 0.125);

		sinus = pow(abs(sinus), 0.1);
		sinus = smoothstep(0.75, 1.0, sinus);
		roaps *= sinus;
	}

	if (roaps < 0.5) {
		roaps = pow(-roaps, 2.0) + 0.25;
	}

	normal = clamp(normal, 0.0, 1.0);
	normal = normal * 2.0 - 1.0;
	if (abs(normal) > 0.2) {
		normal -= pow(sin((abs(normal) - 0.2) * 1.25 * PI * 0.5), 1.1) * sign(normal);
	}
	normal = normal * 0.5 + 0.5;

	float height = vHeight * vHeight;
	vec4 lights = getBackgroundLight();
	float lightness = (lights.r + lights.g + lights.b) / 3.0;
	roaps = mix(0.95, roaps, (height + lightness) / 2.0);
	float luminance = normal * 0.4 * (1.0 - height * 0.5) + height * 0.4 + 0.3;
	vec3 color = vec3(luminance)
				* mix(texture2D(tLights, vec2(1.0 - luminance, 0.9)).rgb, vec3(1.0), lightness * 2.0)
				* calculateSurface(vUv)
				* roaps //+ 0.3;
				+ lights.rgb * 0.3;
	gl_FragColor = vec4(color, 1.0);
}
