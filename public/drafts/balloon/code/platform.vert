varying vec2 vTexCoord;
varying vec3 vViewDir;

void main() {
    vec4 pos = vec4(position,1.0);
    vViewDir = normalize((modelMatrix * pos).xyz - cameraPosition);
    vTexCoord = pos.xz;

    gl_Position = projectionMatrix * modelViewMatrix * pos;
}
