define [

  "text!./platform.vert"
  "text!./platform.frag"

], (vert, frag) ->


  vertexShader: vert
  fragmentShader: frag
  uniforms:
    tDiffuse:
      type: "t"
      value: null
    tLights:
      type: "t"
      value: null
