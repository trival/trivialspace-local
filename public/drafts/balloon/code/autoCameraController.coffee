define [

  "lib/utils/math/functions"
  "lib/utils/math/noise"

], ({sign}
    {noise1D}) ->


  offsetLookX = 23.45566
  offsetLookY = 5.123456
  offsetSpeed = 3.544332
  lookTarget = new THREE.Vector3()
  walkAngle = 0
  walkRadius = 2.5
  currentStep = 15

  lookAt = (camera, angleX, angleY) ->
    angleX = Math.PI / 2 - angleX
    sinX = Math.sin angleX
    position = camera.position
    lookTarget.x = position.x + sinX * Math.cos angleY
    lookTarget.y = position.y + Math.cos angleX
    lookTarget.z = position.z + sinX * Math.sin angleY
    camera.lookAt lookTarget


  noisePow2 = (x) ->
    noise = noise1D x
    noise * noise * sign noise


  updateCamera = (camera, step) ->
    currentStep += step
    stepLookX = (currentStep + offsetLookX) * 0.05
    stepLookY = (currentStep + offsetLookY) * 0.01
    noiseLookX1 = noise1D stepLookX
    noiseLookX2 = noise1D stepLookX * 2
    noiseLookY1 = noise1D stepLookY
    noiseLookY2 = noise1D stepLookY * 2
    noiseMove = noise1D currentStep * 0.05
    stepSpeed = (currentStep + offsetSpeed) * 0.4
    speedNoise = (noise1D stepSpeed) * 0.7 + 0.3
    lookAngleX = noiseLookX1 * Math.PI * 0.5 + noiseLookX2 * Math.PI * 0.4 + 0.1 * Math.PI
    lookAngleY = noiseLookY1 * Math.PI * 2.2 + noiseLookY2 * 1.5 * Math.PI
    
    walkAngle += step * 0.1 * speedNoise
    camera.position.x = Math.cos(walkAngle) * (walkRadius + noiseMove * 2.1) 
    camera.position.z = Math.sin(walkAngle) * (walkRadius + noiseMove * 2.1) 

    lookAt camera, lookAngleX, lookAngleY


  exports = 
    updateCamera: updateCamera