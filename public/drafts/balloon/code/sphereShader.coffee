define [

  "text!./sphere.vert"
  "text!./sphere.frag"

], (vertexShader, fragmentShader) ->


  vertexShader: vertexShader
  fragmentShader: fragmentShader
  uniforms:
    "time":
      type: 'f'
      value: 0
    "tNoise":
      type: 't'
      value: null
    "tLights":
      type: 't'
      value: null

