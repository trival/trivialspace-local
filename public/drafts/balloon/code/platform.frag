uniform sampler2D tDiffuse;
uniform sampler2D tLights;

varying vec2 vTexCoord;
varying vec3 vViewDir;

const vec2 PLATESIZE = vec2(0.3, 0.3);
const vec3 NORMAL = vec3(0.0, 1.0, 0.0);

void main() {
    vec2 position = vTexCoord / PLATESIZE;
    vec2 platePosition = fract(position);
    vec2 plate = floor(position);

    vec2 lightsCoord = vTexCoord * 0.5 + 0.5;
    lightsCoord /= 0.5;
    lightsCoord.x -= 1.0 / 2.0;
    lightsCoord.y -= 1.0 / 1.25;
    lightsCoord -= cameraPosition.xz / 3.8;
    vec3 lightsColor = texture2D(tLights, lightsCoord).rgb * 0.07;
    if (lightsCoord.y > 1.0 / 3.0) {
        lightsColor = vec3(0.0);
    }

    float luminance = pow(0.45 - (abs(vViewDir.y) * 0.45), 2.0);

    vec3 color = texture2D(tDiffuse, platePosition).rgb * 0.3 + 0.15;
    color += lightsColor + luminance * vec3(0.95, 0.98, 1.0);
    gl_FragColor = vec4(color, 1.0);
}

