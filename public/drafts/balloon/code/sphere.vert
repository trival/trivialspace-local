uniform sampler2D tNoise;
uniform float time;

varying vec2 vUv;
varying float vHeight;
varying vec3 vQubeMapUv;

const float PI = 3.14159265359;

vec3 getCubeMapUv(vec3 vertex) {
	vec3 dir = normalize(vertex - cameraPosition * 0.5);
	return dir / max(abs(dir.x), max(abs(dir.y), abs(dir.z)));
}

float circle(vec2 pos, float time, float y, vec4 noise){
	float noiseTime = time * (noise.x - 0.5);
	vec4 noiseX = texture2D(tNoise, uv);
	float wave = sin(pos.x * PI + noiseTime + noise.z * 51.0) * pow(noise.x, 3.0) * y * (1.0 - noiseX.x * 0.3);
	return pow(abs(wave - pos.y - noise.y * sin(noiseTime * 0.5 + noise.y * 37.0)) * (0.5 + y * 0.5), 0.03);
}

void main( void ) {

	vec2 coord = uv * 2.0 - 1.0;
	float y = cos(coord.y * PI * 0.5); 
	float cutStrength = 1.0;
		
	for (int i = 1; i < 20; i++) {
		float index = float(i) / 20.0;
		vec4 noise = texture2D(tNoise, vec2(index, index * 1.3));
		float sinus = circle(coord, time, y, noise);
		sinus = smoothstep(0.75, 1.0, sinus);	
		cutStrength *= sinus;
	}
	if (cutStrength < 0.5) {
		cutStrength = sign(cutStrength) * pow(-cutStrength, 2.0) + 0.25;
	}	

	vUv = uv;
	vec3 pos = position; 
	float normalDeform = 0.0;
	if (cutStrength > 0.0) {
		normalDeform = 1.0 - cutStrength;
	} else {
		normalDeform = -1.0 - cutStrength;
	}

	vHeight = (pos.y + 1.0) * 0.5;
	// float deform = 0.4 + 0.3 * pow(abs(psos.y), 2.0) + 0.3 * cutStrength;
	float deform = 0.6 + 0.5 * abs(cutStrength);
	pos.x *= deform;
	pos.z *= deform;
	pos.y -= cutStrength * 0.1;

	pos = (modelMatrix * vec4(pos, 1.0)).xyz;
	vQubeMapUv = normalize(pos - cameraPosition * 0.5);

	gl_Position = projectionMatrix * viewMatrix * vec4(pos, 1.0);
}