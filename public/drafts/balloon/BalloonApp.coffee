###
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "lib/app/BasicThreeJsApp"
  "lib/app/FirstPersonControls"
  "lib/utils/threejs/helpers"
  "lib/utils/graphics/textureHelpers"

  "draft/code/sphereShader"
  "draft/code/platformShader"
  "draft/code/autoCameraController"
  "text!draft/models/bottom.js"
  "text!draft/models/railing.js"

], (BasicApp
    FirstPersonControls
    {glslTexture, texture}
    {createTileNoiseTexture}
    sphereShader
    platformShader
    {updateCamera}
    bottomModel
    railingModel) ->


  noiseTexture = texture createTileNoiseTexture 512, 512, 5, 5
  noiseTexture.wrapS = noiseTexture.wrapT = THREE.RepeatWrapping

  PLATFORM_SCALE = 3.5


  class BalloonApp extends BasicApp

    textureUrls: [
       PATH_TO_DRAFT + 'textures/lightmap2.jpg'
       PATH_TO_DRAFT + 'textures/beton.jpg'
    ]

    init: ->
      @renderer.setClearColorHex 0x000000, 1

      @camera.fov = 75
      # @camera.position.z = 3.5
      @camera.position.y = -6.4
      @camera.lookAt new THREE.Vector3 0,0,0

      @controls = new FirstPersonControls @camera, @domElement
      @controls.flyMode = true
      @controls.lon = -90
      @controls.manipulateMoveTarget = @manipulateMoveTarget
      @controls.movementSpeed = 0.4

      sphere = new THREE.SphereGeometry 1, 600, 800
      @material = new THREE.ShaderMaterial sphereShader
      @material.side = THREE.DoubleSide
      @material.uniforms.tNoise.value = noiseTexture
      @material.uniforms.tLights.value = @textures[0]

      sphereMesh = new THREE.Mesh sphere, @material
      sphereMesh.scale.multiplyScalar 10
      @scene.add sphereMesh

      platform = @modelLoader.load bottomModel
      platformMaterial = new THREE.ShaderMaterial platformShader
      platformMaterial.side = THREE.DoubleSide
      platformMaterial.uniforms.tDiffuse.value = @textures[1]
      platformMaterial.uniforms.tLights.value = @textures[0]

      platformMesh = new THREE.Mesh platform, platformMaterial
      platformMesh.scale.multiplyScalar PLATFORM_SCALE
      platformMesh.position.y = -7
      @scene.add platformMesh

      railing = @modelLoader.load railingModel
      console.debug railing
      railingMaterial = new THREE.MeshLambertMaterial
        color: 0x999999
        shading: THREE.FlatShading

      railingMesh = new THREE.Mesh railing, railingMaterial
      railingMesh.scale.multiplyScalar PLATFORM_SCALE
      railingMesh.position.y = -7
      @scene.add railingMesh

      pointLight = new THREE.PointLight()
      pointLight.position.y = 0
      pointLight.intensity = 0.35
      ambientLight = new THREE.AmbientLight 0x666666
      @scene.add pointLight
      @scene.add ambientLight


    update: ->
      @material.uniforms.time.value += 0.002
      @controls.update()
      # updateCamera @camera, 0.01

    manipulateMoveTarget: (target) ->
      target.y = 0
      a = (@camera.position.x + target.x) / PLATFORM_SCALE
      b = (@camera.position.z + target.z) / PLATFORM_SCALE
      if a * a + b * b > 1.05
        target.x = -target.x
        target.z = -target.z
