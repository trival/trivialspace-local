__Balloon__ is a virtual space installation inspired by the great work "Big Air Package" by the artist Christo.

Technically, it is an experiment with procedural deformation of geometry and normals to calculate the unique light that can be found inside Christo's original installation. All the work happens in the glsl shader code. It also uses a custom implementation of a cube map texture lookup.

Code and artwork by Thomas Gorny.

## Navigation

* Hold __left mouse button__ and __drag__ mouse to look
* Use __arrows__ or __'wasd'__ or __right mouse button__ to move

## Planned updates

* clean up and document the code!
* increase performance, the draft is very requiring at the moment
* more light tuning

## Changelog

__version 0.1__ (2013-06-07): 

* Initial release
* All planned light calculations implemented
