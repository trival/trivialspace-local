### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [
  "lib/app/BasicThreeJsApp"
  "lib/app/FirstPersonControls"
  "text!draft/code/texture.frag"

  "draft/code/gaussTest"
  "draft/code/particlesTest"
  "draft/code/tiledNoiseTest"

  "js!external/threejs_extras/shaders/CopyShader"
],
( BasicApp, FirstPersonControls,
  fragmentShader,
  {initGaussTest}
  {initParticleTest, updateParticles}
  {initTiledNoiseTest, initNoiseTest}
) ->

  class CanvasTests extends BasicApp

    mesh: null
    controls: null
    trackMouse: true
    projector: new THREE.Projector
    texMouseX: 0
    texMouseY: 0

    canvas: (->
      canvas = document.createElement "canvas"
      canvas.width = 512
      canvas.height = 512
      ctx = canvas.getContext "2d"
      ctx.fillStyle = "black"
      ctx.fillRect 0, 0, canvas.width, canvas.height
      canvas
    )()


    init: ->
      @renderer.setClearColorHex 0x000000, 1
      @renderer.antialias = false

      @camera.position.z = 3.5
      @camera.lookAt new THREE.Vector3 0,0,0

      @controls = new FirstPersonControls @camera, @domElement
      @controls.flyMode = true
      @controls.lon = -90

      @texture = new THREE.Texture @canvas, THREE.UVMapping, THREE.RepeatWrapping, THREE.RepeatWrapping

      plane = new THREE.PlaneGeometry 4, 4, 30, 30
      material = new THREE.ShaderMaterial
        uniforms:
          texture:
            type: 't'
            value: @texture
          tileCount:
            type: 'f'
            value: 1.5
          resolution:
            type: 'v2'
            value: new THREE.Vector2 @width, @height
        vertexShader: THREE.CopyShader.vertexShader
        fragmentShader: fragmentShader

      @mesh = new THREE.Mesh plane, material
      @scene.add @mesh

      # initGaussTest @canvas
      # initParticleTest @canvas
      initTiledNoiseTest @canvas
      # initNoiseTest @canvas
      @texture.needsUpdate = true


    calcTexMouseCoords: ->
      mouseVec = new THREE.Vector3 @mouseX, @mouseY * -1, 1
      @projector.unprojectVector mouseVec, @camera
      ray = new THREE.Raycaster @camera.position, mouseVec.sub(@camera.position).normalize()
      intersects = ray.intersectObject @mesh
      if intersects.length > 0
        {x, y} = intersects[0].point
        @texMouseX = Math.floor ((x + 2) / 4) * @canvas.width
        @texMouseY = @canvas.height - Math.floor ((y + 2) / 4) * @canvas.height


    update: ->
      @calcTexMouseCoords()

      # updateParticles @canvas, [@texMouseX, @texMouseY]

      @texture.needsUpdate = true
      @controls.update()
