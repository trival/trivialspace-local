uniform sampler2D texture;
uniform float tileCount;
uniform vec2 resolution;

varying vec2 vUv;

void main() {
    vec2 uv = vUv * vec2(tileCount);
    /* uv = mod(uv, vec2(20048.0)) / vec2(20048.0); */
    /* if (uv.x > 0.9) uv.x -= 1.0; */
    /* else if (uv.x < 0.1) uv.x += 1.0; */
    /* if (uv.y > 0.9) uv.y -= 1.0; */
    /* else if (uv.y < 0.1) uv.y += 1.0; */
    vec4 texel = texture2D( texture, uv, 0.2 );
    gl_FragColor = texel;

}
