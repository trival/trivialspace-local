define [
  "lib/utils/math/noise"
  "lib/utils/graphics/functions"
  "lib/utils/graphics/imageDataContext"
], ({tileNoise, noise2d},
    {grey}
    {getPixelContext}) ->


  initTiledNoiseTest: (canvas) ->
    {width, height} = canvas
    noise = tileNoise width, height, 5, 5

    ctx = canvas.getContext "2d"
    pixels = getPixelContext ctx.getImageData 0, 0, width, height

    for val, i in noise
      x = i % width
      y = Math.floor i / height
      color = grey Math.floor (val + 1) * 127
      pixels.setColorAt [x, y], color

    ctx.putImageData pixels.imageData, 0, 0


  initNoiseTest: (canvas) ->
    {width, height} = canvas
    ctx = canvas.getContext "2d"
    pixels = getPixelContext ctx.getImageData 0, 0, width, height

    for y in [0..width-1]
      for x in [0..height-1]
        color = grey Math.floor (noise2d(x * 0.01, y * 0.01) + 1) * 127
        pixels.setColorAt [x, y], color

    ctx.putImageData pixels.imageData, 0, 0

