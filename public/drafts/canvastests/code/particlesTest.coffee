define [

  "./Particle"
  "lib/utils/math/functions"
  "lib/utils/math/vectors"
  "lib/utils/physics/functions"
  "lib/utils/graphics/functions"
  "lib/utils/graphics/imageDataContext"

], (Particle,
    {normalRand},
    {isEqual}
    {gravity, drag},
    {grey, colorToCSS, newCanvas},
    {getRepeatedPixelContext, getPixelContext}) ->


  particles = []


  mouse =
    position: [0, 0]
    mass: 2000


  initParticles = (canvas) ->

    ctx = canvas.getContext "2d"
    ctx.fillStyle = "white"
    ctx.fillRect 0, 0, canvas.width, canvas.height

    particles = for i in [1..70]
      particle = new Particle canvas.width, canvas.height, Math.floor((normalRand() + 5) * 20)
      console.debug particle
      [c, ctx] = newCanvas 10, 10, (ctx) ->
        grd = ctx.createRadialGradient 5, 5, 1, 5, 5, 5
        grd.addColorStop 0, colorToCSS grey particle.mass, 200
        grd.addColorStop 1, colorToCSS grey particle.mass, 0
        ctx.fillStyle = grd
        ctx.fillRect 0, 0, 10, 10
      particle.img = getPixelContext ctx.getImageData 0, 0, 10, 10
      particle


  updateParticles = (canvas, mouseCoords) ->

    ctx = canvas.getContext "2d"
    pixels = getRepeatedPixelContext ctx.getImageData 0, 0, canvas.width, canvas.height
    pixels.increaseAllBy [5, 5, 5, 0]
    mouse.position = mouseCoords

    for particle in particles
      particle.applyForce gravity mouse, particle, 1
      particle.applyForce drag particle, 0.7
      for particle2 in particles
        unless particle is particle2
          particle.applyForce gravity particle2, particle, 0.5
      particle.update()
      [x, y] = particle.position
      pixels.drawImageAt particle.img, [x-5, y-5]

    ctx.putImageData pixels.imageData, 0, 0


  exports =
    initParticleTest: initParticles
    updateParticles: updateParticles
