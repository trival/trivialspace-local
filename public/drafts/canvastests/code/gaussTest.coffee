define [

  "lib/utils/math/functions"
  "lib/utils/graphics/imageDataContext"

], ({normalRand},
    {getRepeatedPixelContext}) ->


  initGaussTest: (canvas) ->
    deviation = (0 for i in [1..512])
    for i in [0..100000]
      val = Math.floor((normalRand() + 3.0) * (512 / 6))
      deviation[val] -= 1

    ctx = canvas.getContext "2d"
    ctx.fillStyle = "white"
    ctx.fillRect 0, 0, canvas.width, canvas.height

    imgData = ctx.getImageData 0, 0, canvas.width, canvas.height
    pixels = getRepeatedPixelContext imgData

    pixels.setColorAt [x, y], [0, 0, 0, 255] for y, x in deviation

    ctx.putImageData pixels.imageData, 0, 0
