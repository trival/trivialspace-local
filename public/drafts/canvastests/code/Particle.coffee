### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "lib/utils/math/functions"
  "lib/utils/math/vectors"

], ({randInt},
    {add, div, limit}) ->


  class Particle

    position: [0, 0]
    velocity: [0, 0]
    acceleration: [0, 0]
    mass: 0
    maxWidth: 0
    maxHeight: 0

    constructor: (width, height, @mass) ->
      @position = [randInt(width), randInt(height)]
      @maxWidth = width
      @maxHeight = height

    update: ->
      @velocity = add @velocity, @acceleration
      @velocity = limit @velocity, 8
      @position = add @position, @velocity
      @adjustPosition()
      @acceleration = [0, 0]

    applyForce: (force) ->
      @acceleration = add @acceleration, div force, @mass

    adjustPosition: ->
      [x, y] = @position
      x -= @maxWidth if x >= @maxWidth
      y -= @maxHeight if y >= @maxHeight
      x += @maxWidth if x < 0
      y += @maxHeight if y < 0
      @position = [Math.floor(x), Math.floor(y)]

