uniform sampler2D tLight; 
uniform vec3 lights;
uniform vec3 color1;
uniform vec3 color2;
uniform vec3 color3;
uniform vec3 intensity;
varying vec2 vUv;

void main(void) {

    vec4 light = texture2D(tLight, vUv);
    vec3 lightColor = vec3(0.0);
    if (lights.r == 1.0) lightColor += light.g * (color1 + 0.5) * intensity.x;
    if (lights.g == 1.0) lightColor += light.b * (color2 + 0.5) * intensity.y;
    if (lights.b == 1.0) lightColor += light.r * (color3 + 0.5) * intensity.z;
    lightColor *= 2.0 * light.a;
    gl_FragColor = vec4(lightColor, 1.0);
}
