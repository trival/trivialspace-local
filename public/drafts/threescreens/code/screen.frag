uniform float time;
uniform vec3 color;
uniform float intensity;

// noise effect intensity value (0 = no effect, 1 = full effect)
const float nIntensity = 0.8;

// scanlines effect intensity value (0 = no effect, 1 = full effect)
const float sIntensity = 0.1;

// scanlines effect count value (0 = no effect, 4096 = full effect)
const float sCount = 4096.0;

varying vec2 vUv;

void main() {

    // make some noise
    float x = vUv.x * vUv.y * time * 1000.0;
    x = mod( x, 13.0 ) * mod( x, 123.0 );
    float dx = mod( x, 0.01 );

    // add noise
    vec3 cResult = (color - 0.2) + color * clamp( 0.1 + dx * 100.0, 0.0, 1.0 );

    // get us a sine and cosine
    vec2 sc = vec2( sin( vUv.y * sCount ), cos( vUv.y * sCount ) );

    // add scanlines
    cResult += color * vec3( sc.x, sc.y, sc.x ) * sIntensity;

    // interpolate between source and result by intensity
    cResult = color + clamp( nIntensity, 0.0,1.0 ) * ( cResult - color );

    cResult *= (intensity - 0.2);
    cResult += 0.3;

    gl_FragColor =  vec4( cResult, 1.0 );

}

