__Imperator kino__ is another approach to indirect lighting. The illumination effects are being read from a precomputed texture, which stores the light of each of the screens in one of its color channels. Color and intensity are then computed and applied in real time in sync with the screen flickering.

Code and artwork by Thomas Gorny.

## Navigation

* Hold __left mouse button__ and __drag__ mouse to look
* Use __arrows__ or __'wasd'__ or __right mouse button__ to move

## Planned updates

* experiment with depth of field
* more light tuning
* add 3D sound and synchronize it with the flickering screens

## Changelog

__version 0.1__ (2013-04-07): 

* Initial release
* Synchronication of ground light texture and procedural screen illumination
