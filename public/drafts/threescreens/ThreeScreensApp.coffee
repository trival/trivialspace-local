###
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "lib/app/BasicThreeJsApp"
  "lib/app/FirstPersonControls"
  "draft/code/AutoCameraController"

  "lib/utils/math/functions"
  "lib/utils/sound/functions"

  "jquery"

  "text!draft/code/light.vert"
  "text!draft/code/light.frag"
  "text!draft/code/screen.frag"

  "text!draft/models/bottom.js"

], (BasicApp, FirstPersonControls, {updateCamera}
    {randInt, normalRand},
    {source}
    $,
    lightVertexShader, lightFragmentShader, screenFragmentShader,
    bottomModel) ->


  screensOn = new THREE.Vector3 1, 1, 1
  screensChanged = new THREE.Vector3 1, 1, 1

  intensityUniform =
    type: 'v3'
    value: new THREE.Vector3

  time =
    type: 'f'
    value: 0.05

  changeColor = (color) ->
    color.x = normalRand() / 12 + 0.7
    color.y = normalRand() / 12 + 0.7
    color.z = normalRand() / 12 + 0.7
    color

  screenColors = [changeColor(new THREE.Vector3),
                  changeColor(new THREE.Vector3),
                  changeColor(new THREE.Vector3)]

  startScreenSwitching = (screenVar, colorIndex) ->
    toggleScreen = ->
      screensOn[screenVar] = if screensOn[screenVar] then 0 else 1
      changeColor screenColors[colorIndex] unless screensOn[screenVar]
      screensChanged[screenVar] = 1
      setTimeout toggleScreen, (Math.random() * 20 + 1) * 1000 #(normalRand() + 3) * 1500
    toggleScreen()


  pick3 = (array) ->
    set = {}
    pic = []
    set[randInt(2.99)] = true
    set[randInt(2.99)] = true
    set[randInt(2.99)] = true
    pic.push array[0] if set[0]
    pic.push array[1] if set[1]
    pic.push array[2] if set[2]
    console.debug set
    pic


  class ThreeScreensApp extends BasicApp

    mesh: null
    controls: null
    composer: null
    playSound: false


    textureUrls: [ PATH_TO_DRAFT + "textures/lightmap.png" ]
    soundUrls: [
      PATH_TO_DRAFT + "sounds/Bildschirm_1_1.ogg"
      PATH_TO_DRAFT + "sounds/Bildschirm_1_2.ogg"
      PATH_TO_DRAFT + "sounds/Bildschirm_1_3.ogg"
      PATH_TO_DRAFT + "sounds/Bildschirm_2_1.ogg"
      PATH_TO_DRAFT + "sounds/Bildschirm_2_2.ogg"
      PATH_TO_DRAFT + "sounds/Bildschirm_2_3.ogg"
      PATH_TO_DRAFT + "sounds/Bildschirm_3_1.ogg"
      PATH_TO_DRAFT + "sounds/Bildschirm_3_2.ogg"
      PATH_TO_DRAFT + "sounds/Bildschirm_3_3.ogg"
    ]


    init: ->

      @playSound = @sounds.length > 0

      # @directRender = false
      @renderer.setClearColorHex 0, 1
      # @renderer.autoClear = false

      @camera.position.set 3, 1, 0
      @camera.lookAt new THREE.Vector3 0, 1, 0

      @controls = new FirstPersonControls(@camera, @domElement)
      @controls.manipulateMoveTarget = @manipulateMoveTarget
      @controls.movementSpeed = 0.5
      @controls.lon = 180
      # @controls.flyMode = true

      screenGeometry = new THREE.PlaneGeometry 1.9, 1.3, 5, 4
      # screenGeometry = new THREE.SphereGeometry 0.2, 10, 10

      createScreenMat = (color)->
        new THREE.ShaderMaterial
          side: THREE.DoubleSide
          vertexShader: lightVertexShader
          fragmentShader: screenFragmentShader
          uniforms:
            time: time
            color:
              type: 'v3'
              value: color
            intensity:
              type: 'f'
              value: 0

      @screenMatLight1 = createScreenMat screenColors[0]
      @screenMatLight2 = createScreenMat screenColors[1]
      @screenMatLight3 = createScreenMat screenColors[2]

      console.debug @screenMatLight1

      @screen1 = new THREE.Mesh screenGeometry, @screenMatLight1
      @screen1.position.y = 1
      @screen1.position.z = 0.3
      @scene.add @screen1

      @screen2 = new THREE.Mesh screenGeometry, @screenMatLight2
      @screen2.rotation.y = Math.PI * 2 / 3
      @screen2.position.y = 1
      @screen2.position.x = 0.55
      @screen2.position.z = -0.65
      @scene.add @screen2

      @screen3 = new THREE.Mesh screenGeometry, @screenMatLight3
      @screen3.rotation.y = -Math.PI * 2 / 3
      @screen3.position.y = 1
      @screen3.position.x = -0.55
      @screen3.position.z = -0.65
      @scene.add @screen3

      bottomTex = @textures[0]
      bottomTex.generateMipmaps = false
      bottomTex.minFilter = THREE.LinearFilter
      @bottomMat = new THREE.ShaderMaterial
        uniforms:
          tLight:
            type: 't'
            value: bottomTex
          lights:
            type: 'v3'
            value: screensOn
          color1:
            type: 'v3'
            value: screenColors[0]
          color2:
            type: 'v3'
            value: screenColors[1]
          color3:
            type: 'v3'
            value: screenColors[2]
          intensity: intensityUniform


        vertexShader: lightVertexShader
        fragmentShader: lightFragmentShader

      bottom = new THREE.Mesh @modelLoader.load(bottomModel), @bottomMat
      @scene.add bottom

      @screenMatDark = new THREE.MeshBasicMaterial color: 0x090909, side: THREE.DoubleSide
      @screenMatBlack = new THREE.MeshBasicMaterial color: 0x0, side: THREE.DoubleSide

      screenBack1 = new THREE.Mesh screenGeometry, @screenMatDark
      screenBack1.position.y = 1
      screenBack1.position.z = 0.29
      @scene.add screenBack1

      screenBack2 = new THREE.Mesh screenGeometry, @screenMatDark
      screenBack2.rotation.y = Math.PI * 2 / 3
      screenBack2.position.y = 1
      screenBack2.position.x = 0.54
      screenBack2.position.z = -0.64
      @scene.add screenBack2

      screenBack3 = new THREE.Mesh screenGeometry, @screenMatDark
      screenBack3.rotation.y = -Math.PI * 2 / 3
      screenBack3.position.y = 1
      screenBack3.position.x = -0.54
      screenBack3.position.z = -0.64
      @scene.add screenBack3

      setTimeout (-> startScreenSwitching 'x', 0), 5000
      setTimeout (-> startScreenSwitching 'y', 1), 7000
      setTimeout (-> startScreenSwitching 'z', 2), 4000

      if @playSound
        c = @soundContext
        @sounds1 = [@sounds[0], @sounds[1], @sounds[2]]
        @sounds2 = [@sounds[3], @sounds[4], @sounds[5]]
        @sounds3 = [@sounds[6], @sounds[7], @sounds[8]]
        @currSounds1 = pick3 @sounds1
        @currSounds2 = pick3 @sounds2
        @currSounds3 = pick3 @sounds3
        pos1 = @screen1.position
        pos2 = @screen2.position
        pos3 = @screen3.position
        pann1 = c.createPanner()
        pann1.refDistance = 0.5
        pann1.connect @soundDestination
        pann1.setPosition pos1.x, pos1.y, pos1.z * 2
        pann2 = c.createPanner()
        pann2.refDistance = 0.5
        pann2.connect @soundDestination
        pann2.setPosition pos2.x * 1.4, pos2.y, pos2.z * 1.3
        pann3 = c.createPanner()
        pann3.refDistance = 0.5
        pann3.connect @soundDestination
        pann3.setPosition pos3.x * 1.4, pos3.y, pos3.z * 1.3
        @gain1 = c.createGain()
        @gain1.connect pann1
        @gain2 = c.createGain()
        @gain2.connect pann2
        @gain3 = c.createGain()
        @gain3.connect pann3


    update: ->
      updateCamera @camera, 0.005

      @screen1.material = if screensOn.x then @screenMatLight1 else @screenMatBlack
      @screen2.material = if screensOn.y then @screenMatLight2 else @screenMatBlack
      @screen3.material = if screensOn.z then @screenMatLight3 else @screenMatBlack

      intensity = intensityUniform.value
      intensity.x = normalRand() * 0.1 + 0.9
      intensity.y = normalRand() * 0.1 + 0.9
      intensity.z = normalRand() * 0.1 + 0.9

      @screenMatLight1.uniforms.intensity.value = intensity.x
      @screenMatLight2.uniforms.intensity.value = intensity.y
      @screenMatLight3.uniforms.intensity.value = intensity.z

      time.value += 0.001

      if @playSound
        console.debug "playSound"
        c = @soundContext
        pos = @camera.position
        c.listener.setPosition pos.x, pos.y, pos.z
        @gain1.gain.value = (intensity.x - 0.3) * 1.43
        @gain2.gain.value = (intensity.y - 0.3) * 1.43
        @gain3.gain.value = (intensity.z - 0.3) * 1.43
        for pos, index in ['x', 'y', 'z']
          if screensChanged[pos]
            screensChanged[pos] = 0
            if screensOn[pos]
              arr = pick3 @['sounds'+(index+1)]
              s = for sound in arr
                p = source sound, c
                p.loop = true
                p.connect @["gain"+(index+1)]
                p.start 0, sound.duration * Math.random()
                p
              @["sound"+(index+1)] = s

            else
              s = @["sound" + (index+1)]
              for sound in s
                sound.stop 0
                sound.disconnect @["gain"+(index+1)]


    manipulateMoveTarget: (target) =>
      target.y = 0
      # target.x = 0 if 0.1 > @camera.position.x - target.x
      # target.z = 0 if 0.1 > @camera.position.z - target.z
