define [
  "text!./plane.vert"
  "text!./plane.frag"
], (vertexShader, fragmentShader) ->

  exports =
    vertexShader: vertexShader
    fragmentShader: fragmentShader
    uniforms:
      "tNoise":
        type: 't'
        value: null
      "resolution":
        type: 'v2'
        value: null