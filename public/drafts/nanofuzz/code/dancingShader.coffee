define [
  "text!./dancing.vert"
  "text!./dancing.frag"
], (vertexShader, fragmentShader) ->

  exports =
    vertexShader: vertexShader
    fragmentShader: fragmentShader
    uniforms:
      "time":
        type: 'f'
        value: null
      "random":
        type: 'v4'
        value: null
      "tNoise":
        type: 't'
        value: null
      "resolution":
        type: 'v2'
        value: null
