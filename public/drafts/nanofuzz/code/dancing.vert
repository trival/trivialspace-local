uniform float time;
uniform vec4 random;
uniform mat4 rotationMatrix;

varying vec3 vNormal;
varying float vHeight;
varying vec2 vUv;
varying vec3 vViewDir;

void main(void) {
    vec4 displace = vec4(0.0);
    vec4 pos = modelMatrix * rotationMatrix * vec4(position, 1.0);
    vNormal = normal;

    vec4 wave = random;
    wave.xz += 1.0;
    wave.xz *= 2.5;
    wave.yw += 0.5;
    wave.yw *= 0.5;
    float root = clamp(pos.y, 0.0, 5.0);
    displace.x = sin(time * wave.y - pos.y / wave.x) * wave.y * (root / 5.0);
    displace.z = sin(time * wave.w - pos.y / wave.z) * wave.w * (root / 5.0);

    root = 5.0 - root;
    if (root > 0.1) {
        displace += vec4(normal, 0.0) * root * root * root / 25.0;
        vNormal = mix(vNormal, vec3(0.0, 1.0, 0.0), (root / 5.0));
    }

    vUv = uv;
    vNormal = normalMatrix * vNormal;
    pos = pos + displace;
    vViewDir = (viewMatrix * pos).xyz * -1.0;
    vHeight = pos.y; 
    gl_Position = projectionMatrix * viewMatrix * pos;
}


