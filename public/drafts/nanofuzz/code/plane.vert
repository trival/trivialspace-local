varying vec3 vNormal;
varying vec2 vUv;
varying vec3 vViewDir;

void main(void) {
	vUv = uv;
    vNormal = normalMatrix * vec3(0.0, 0.0, 1.0);
    vec4 pos = modelViewMatrix * vec4(position, 1.0);
    vViewDir = normalize(pos.xyz * -1.0);
    gl_Position = projectionMatrix * pos;
}


