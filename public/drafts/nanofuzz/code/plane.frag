uniform sampler2D tNoise;
uniform vec2 resolution;

varying vec3 vNormal;
varying vec2 vUv;
varying vec3 vViewDir;


void main() {
	float depth = (60.0 - gl_FragCoord.z / gl_FragCoord.w) / 60.0;
    vec2 uv = vUv;

    vec2 screen = gl_FragCoord.xy / resolution;

	uv *= 7.0;
	vec4 cutTex = texture2D(tNoise, uv);
	vec4 blurTex = texture2D(tNoise, uv);
	vec4 noiseTex = texture2D(tNoise, uv);
	vec4 noiseTexV = texture2D(tNoise, uv + vec2(0.0, 1.0 / resolution.y));
	vec4 noiseTexH = texture2D(tNoise, uv + vec2(1.0 / resolution.x, 0.0));
	cutTex = (cutTex - 0.5) * 2.0;
	blurTex = (blurTex - 0.5) * 2.0;
	float noiseColor = noiseTex.r * 0.55 + noiseTex.g * 0.25 + noiseTex.b * 0.13 + noiseTex.a * 0.07;
	float noiseColorV = noiseTexV.r * 0.55 + noiseTexV.g * 0.25 + noiseTexV.b * 0.13 + noiseTexV.a * 0.07;
	float noiseColorH = noiseTexH.r * 0.55 + noiseTexH.g * 0.25 + noiseTexH.b * 0.13 + noiseTexH.a * 0.07;
	noiseColor = pow(1.0 - abs(noiseColor * 2.0 - 1.0), 6.0);
	noiseColorH = pow(1.0 - abs(noiseColorH * 2.0 - 1.0), 6.0);
	noiseColorV = pow(1.0 - abs(noiseColorV * 2.0 - 1.0), 6.0);
	float cutColor = cutTex.g * 0.55 + cutTex.b * 0.30 + cutTex.a * 0.15;
	float negcutColor = 1.0;

	if (cutColor < 0.0) {
		negcutColor += cutColor;
		cutColor = 1.0;
	} else {
		cutColor = 1.0 - cutColor;
	}

	cutColor = pow(cutColor, 50.0);
	negcutColor = pow(negcutColor, 50.0);
	if (negcutColor < 1.0) cutColor = negcutColor * -1.0;
	cutColor *= clamp(blurTex.w + blurTex.y + blurTex.z + 0.3, 0.0, 1.0);

	vec3 viewDir = vViewDir;
	vec3 normal = vNormal;	
	normal.x -= (noiseColor - noiseColorH) * 2.0;
	normal.y += (noiseColor - noiseColorV) * 2.0;
	normal = normalize(normal);
    vec3 rotX = normalize(vec3(0.0, viewDir.yz));
    vec3 rotY = normalize(vec3(viewDir.x, 0.0, viewDir.z));
    // computed rotation by wolfram alpha: 
    // (-c x+a z, -a x Y-c Y z+b Z, b Y+a x Z+c z Z)
    normal = vec3(-normal.z * rotY.x + normal.x * rotY.z,
                    -normal.x * rotY.x * rotX.y - normal.z * rotX.y * rotY.z + normal.y * rotX.z,
                    normal.y * rotX.y + normal.x * rotY.x * rotX.z + normal.z * rotY.z * rotX.z);
    normal = abs(normal);

	vec3 color = vec3(0.3);
	color += (1.0 - normal.z) * 0.7;
	color -= pow((noiseColorV + noiseColorH) / 2.0, 3.0) * 0.1;
	color += cutColor * 0.4;
	color = mix(vec3(0.07), color, depth);

	gl_FragColor = vec4(color, 1.0);
	// gl_FragColor = vec4(normal, 1.0);
}
