###
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "draft/code/dancingShader"
  "draft/code/bottomShader"
  "draft/code/autoCameraController"
  "lib/app/BasicThreeJsApp"
  "lib/app/FirstPersonControls"
  "lib/utils/math/functions"
  "lib/utils/threejs/helpers"
  "lib/utils/graphics/textureHelpers"
  "jquery"

  "js!external/threejs_extras/shaders/FXAAShader"
  "js!external/threejs_extras/shaders/BokehShader"
  "js!external/threejs_extras/shaders/CopyShader"
  "js!external/threejs_extras/shaders/DotScreenShader"
  "js!external/threejs_extras/shaders/FilmShader"
  "js!external/threejs_extras/shaders/VignetteShader"
  "js!external/threejs_extras/shaders/HorizontalBlurShader"
  "js!external/threejs_extras/shaders/VerticalBlurShader"
  "js!external/threejs_extras/postprocessing/ShaderPass"
  "js!external/threejs_extras/postprocessing/MaskPass"
  "js!external/threejs_extras/postprocessing/DotScreenPass"
  "js!external/threejs_extras/postprocessing/FilmPass"
  "js!external/threejs_extras/postprocessing/BloomPass"
  "js!external/threejs_extras/postprocessing/RenderPass"
  "js!external/threejs_extras/postprocessing/EffectComposer"

], (dancingShader, bottomShader
    {updateCamera}
    BasicApp, FirstPersonControls
    {randInt}
    {renderTarget}
    {createTileNoiseTexture}
    $) ->


  class NanoFuzzApp extends BasicApp

    time:
      type: 'f'
      value: 0

    resolution: new THREE.Vector2

    startTime: Date.now()


    init: ->
      @directRender = false
      @renderer.autoClear = false
      @renderer.setClearColorHex 0x101010, 1
      @camera.far = 60
      @camera.updateProjectionMatrix()
      @camera.position.y = 7

      @resolution.set @width, @height

      stickTextureCanvas = createTileNoiseTexture 512, 512, 10, 30
      stickTexture = new THREE.Texture stickTextureCanvas, new THREE.UVMapping(), THREE.RepeatWrapping, THREE.RepeatWrapping
      stickTexture.needsUpdate = true

      bottomTextureCanvas = createTileNoiseTexture 512, 512, 20, 20
      bottomTexture = new THREE.Texture bottomTextureCanvas, new THREE.UVMapping(), THREE.RepeatWrapping, THREE.RepeatWrapping
      bottomTexture.needsUpdate = true

      # $(document.body).append stickTexture.image
      # $(document.body).append bottomTexture.image

      planeMaterial = new THREE.ShaderMaterial bottomShader
      planeMaterial.uniforms.tNoise.value = bottomTexture
      planeMaterial.uniforms.resolution.value = @resolution

      planeGeometry = new THREE.PlaneGeometry 300, 300, 70, 70
      @plane = new THREE.Mesh planeGeometry, planeMaterial
      @plane.rotation.x = -Math.PI / 2
      @scene.add @plane
      console.debug @plane.material

      cylinderGeometry = new THREE.CylinderGeometry 1, 1, 50, 50, 140, true

      @sticks = for i in [0..500]
        do =>
          randomVector = new THREE.Vector4 Math.random(), Math.random(), Math.random(), Math.random()
          # material = new THREE.MeshBasicMaterial map: stickTexture
          material = new THREE.ShaderMaterial
            fragmentShader: dancingShader.fragmentShader
            vertexShader: dancingShader.vertexShader
            uniforms: THREE.UniformsUtils.clone dancingShader.uniforms
            transparent: true
          material.uniforms.random.value = randomVector
          material.uniforms.time = @time
          material.uniforms.tNoise.value = stickTexture
          material.uniforms.resolution.value = @resolution

          rotation = new THREE.Vector3 Math.random() * .5 - .25, 0, Math.random() * .5 - .25
          rotationMatrix = new THREE.Matrix4
          rotationMatrix.setRotationFromEuler rotation
          material.uniforms.rotationMatrix =
            type: 'm4'
            value: rotationMatrix

          cylinder = new THREE.Mesh cylinderGeometry, material
          cylinder.position.x = randInt(300) - 150
          cylinder.position.y = 23.5
          cylinder.position.z = randInt(200) - 100
          @scene.add cylinder
          cylinder

      @controls = new FirstPersonControls @camera, @domElement
      # @controls.flyMode = true
      @controls.manipulateMoveTarget = @manipulateMoveTarget
      @controls.movementSpeed = 2

      renderedScene = new THREE.RenderPass @scene, @camera
      @effectFXAA = new THREE.ShaderPass THREE.FXAAShader
      @effectFXAA.uniforms.resolution.value.set 1 / @width, 1 / @height
      effectHBlur = new THREE.ShaderPass THREE.HorizontalBlurShader
      effectHBlur.uniforms.h.value = 1 / 1.5 / @height
      effectVBlur = new THREE.ShaderPass THREE.VerticalBlurShader
      effectVBlur.uniforms.v.value = 1 / 1.5 / @width
      effectFilm = new THREE.FilmPass
      effectVignette = new THREE.ShaderPass THREE.VignetteShader
      effectVignette.renderToScreen = true

      @composer = new THREE.EffectComposer @renderer, renderTarget @width, @height, THREE.RGBAFormat
      @composer.addPass renderedScene
      @composer.addPass @effectFXAA
      @composer.addPass effectFilm
      @composer.addPass effectHBlur
      @composer.addPass effectVBlur
      @composer.addPass effectFilm
      @composer.addPass effectVignette

      console.debug @scene


    update: ->
      @time.value = (Date.now() - @startTime) / 660
      # @controls.update()
      updateCamera @camera, 0.01
      @composer.render 0.01


    manipulateMoveTarget: (target) =>
      target.y = 0


