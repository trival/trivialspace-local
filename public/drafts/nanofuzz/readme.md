__Nano fuzz__ started as vertex shader animation experiment, but became a trip close onto the surface of your skin. This draft generates its microscopic view of the world fully procedurally. It uses a precomputed noise texture that is read out in different ways in the fragment shader to create the surface appearence. Deformation and animation is applied to the hair geometry in the vertex shader. Some postprocessing provided by [three.js](http://threejs.org/) is used to cover up the banality of the look. 

Code by Thomas Gorny.

## Navigation

* Hold __left mouse button__ and __drag__ mouse to look
* Use __arrows__ or __'wasd'__ or __right mouse button__ to move

## Planned updates

* Improve general aesthetics
* Make ground infinite and spherical
* More finetuning of the surface structure

## Changelog

__version 0.2__ (2013-04-27):

* Better contact realisation between hair and ground
* Added noise and deformation textures
* Simulation of microscope lighting conditions

__version 0.1__ (2012-12-06): 

* Initial release
* Deformation and Animation of the hairs in the vertex shader
* Basic lighting implementation 
