### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "draft/code/shader"
  "lib/utils/js/functions"
  "js!external/threejs_extras/Detector"

], ({fragmentSource},
    {extend}) ->


  class RawShaderApp

    domElement: null
    paused: false
    fullscreen: false
    width: 800
    height: 600
    noWebGL: false
    mouseX: 0
    mouseY: 0
    trackMouse: false
    startTime: new Date("2012-11-27").getTime()


    init: ->
      gl = @canvas.getContext 'experimental-webgl', preserveDrawingBuffer: true

      fragmentShader = gl.createShader gl.FRAGMENT_SHADER
      gl.shaderSource fragmentShader, fragmentSource
      gl.compileShader fragmentShader
      unless gl.getShaderParameter fragmentShader, gl.COMPILE_STATUS
        console.error "Es ist ein Fehler beim Kompilieren der Shader aufgetaucht: " + gl.getShaderInfoLog fragmentShader

      vertexShader =  gl.createShader gl.VERTEX_SHADER
      gl.shaderSource vertexShader, 'attribute vec3 position; void main() { gl_Position = vec4( position, 1.0 ); }'
      gl.compileShader vertexShader
      unless gl.getShaderParameter vertexShader, gl.COMPILE_STATUS
        console.error "Es ist ein Fehler beim Kompilieren der Shader aufgetaucht: " + gl.getShaderInfoLog vertexShader

      @shaderProgram = gl.createProgram()
      gl.attachShader @shaderProgram, vertexShader
      gl.attachShader @shaderProgram, fragmentShader
      gl.linkProgram @shaderProgram


      unless gl.getProgramParameter @shaderProgram, gl.LINK_STATUS
        console.error "Initialisierung des Shaderprogramms nicht möglich."

      gl.useProgram @shaderProgram

      @vertexPositionAttribute = gl.getAttribLocation @shaderProgram, "position"
      gl.enableVertexAttribArray @vertexPositionAttribute

      @squareVerticesBuffer = gl.createBuffer()
      gl.bindBuffer gl.ARRAY_BUFFER, @squareVerticesBuffer

      vertices = [
       1.0,  1.0,  0.0,
       -1.0, 1.0,  0.0,
       1.0,  -1.0, 0.0,
       -1.0, -1.0, 0.0
      ]

      gl.bufferData gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW


    render: ->
      time = Date.now() - @startTime

      gl = @canvas.getContext 'experimental-webgl', preserveDrawingBuffer: true
      gl.clear gl.COLOR_BUFFER_BIT
      gl.bindBuffer gl.ARRAY_BUFFER, @squareVerticesBuffer
      gl.vertexAttribPointer @vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0

      resolutionUniform = gl.getUniformLocation @shaderProgram, "resolution"
      gl.uniform2f resolutionUniform, @width, @height
      timeUniform = gl.getUniformLocation @shaderProgram, "time"
      gl.uniform1f timeUniform, time / 1000

      gl.drawArrays gl.TRIANGLE_STRIP, 0, 4


    start: ->
      unless @noWebGL
        @_internalInit()
        @init()
        @_animate()
        @dispatchEvent type: 'draftLoaded'


    constructor: (configObject) ->

      extend @, configObject
      THREE.EventDispatcher.call this

      if typeof @domElement is 'string'
        @domElement = document.getElementById @domElement
      else unless @domElement
        @domElement = document.createElement "div"
        document.body.appendChild @domElement

      if @fullscreen
        @height = window.innerHeight
        @width = window.innerWidth
        document.body.style.overflow = 'hidden'
      else
        @domElement.style.width = @width + 'px'
        @domElement.style.height = @height + 'px'

      unless Detector.webgl
        @noWebGL = true
        Detector.addGetWebGLMessage parent: @domElement
      else
        @canvas = document.createElement 'canvas'
        @canvas.width = @width
        @canvas.height = @height
        @domElement.appendChild @canvas



    _animate: =>
      unless @paused
        # requestAnimationFrame @_animate
        @render()


    _internalInit: ->
      if @trackMouse
        @domElement.addEventListener 'mousemove', (evt) =>
          rect = @domElement.getBoundingClientRect()
          x = evt.clientX - rect.left
          y = evt.clientY - rect.top
          @mouseX = (x / @width) * 2 - 1
          @mouseY = (y / @height) * 2 - 1

      document.addEventListener "keypress", (evt) =>
        key = evt.charCode
        if "p".charCodeAt(0) is key
          @paused = not @paused
          @_animate()  unless @paused

      if @fullscreen
        window.addEventListener "resize", @onResize
        document.addEventListener "keyup", (event) ->
          if event.keyCode == 27 #ESC
            top.history.back()

      else
        @domElement.addEventListener "resize", @onResize


    onResize: (event) =>
      console.debug "resize event"
      @width = if @fullscreen then window.innerWidth else @domElement.innerWidth
      @height = if @fullscreen then window.innerHeight else @domElement.innerHeight
      console.debug "width: " + @width
      console.debug "height: " + @height
      @canvas.width = @width
      @canvas.height = @height

      gl = @canvas.getContext 'experimental-webgl', preserveDrawingBuffer: true
      gl.viewport 0, 0, @width, @height
