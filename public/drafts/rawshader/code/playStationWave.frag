uniform vec2 resolution;
uniform float time;

void main(void)
{
	vec2 uPos = ( gl_FragCoord.xy / resolution.xy );//normalize wrt y axis
    uPos -= .5;
	vec3 color = vec3(0.0);
    float sinY = uPos.y + sin( uPos.x * 4. + time * 0.5) * 0.1;
    float fTemp = smoothstep(0.0, 1.0, clamp(abs(1.0 / (sinY * 8. * (snoise(vec3(uPos.x, time * 0.2, 0.0)) * 0.2 + 0.6))) - 1., 0.0, 1.0));
    fTemp *= pow(1.0 - abs(snoise(vec3(uPos.x * 2., sinY * 5., time * 0.05))), 2.0);
    //color += vec3( fTemp, fTemp/10.0, pow(fTemp,0.9)*1.5 ); 
    /* float fTemp = snoise(uPos * 4.0) * 0.5 + 0.5; */
    //color += vec3( fTemp );
	gl_FragColor = vec4(smoothstep(0.0,1.3,fTemp));
}
