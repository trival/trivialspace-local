uniform vec2 resolution;
uniform float time;

void main(void)
{
    vec2 pos = gl_FragCoord.xy;
    pos -= resolution / 2.0;
    /* pos *= .02; */

    /* float col = abs(sin(pos.x) + sin(pos.y) - 0.5); */

    /* float col = sin(pos.x) + sin(pos.x * 3.5) * 0.5 + sin(pos.x * 9.3) * 0.2; */
    /* col *= 0.3; */
    /* col += 0.5; */
    /* float colY = cos(pos.y * 3.0) + cos(pos.y * 4.3) * 0.6 + cos(pos.y * 7.8) * 0.1; */
    /* colY *= 0.2; */
    /* colY += 0.5; */
    /* col *= colY; */

    /* float col = sin(pos.x) * sin(pos.y * -.5) + cos(pos.y) * cos(pos.x) ; */

    float col = pos.y - 200.0;
    col += sin(pos.x * 0.05) * 50.0;
    col = abs(col) - 1.0;
    
    // smooth out border
    /* col /= resolution.x * resolution.y * 0.5; */
    /* col = sqrt(col); */

    // invert color
    // col = 1.0 - col;

	gl_FragColor = vec4(vec3(col), 1.0);
}

