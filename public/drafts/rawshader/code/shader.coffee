define [
  "text!assets/shaderlibs/noise3D.glsl"
  "text!./circle.frag"
], (noise, frag) ->

  fragmentSource: "precision highp float;\n" + noise + "\n" + frag
