uniform vec2 resolution;
uniform float time;

const float r = 300.0;
const vec2 center = vec2(100.0, -50.0);
const float strokeWidth = 10.0;

void main(void)
{
    vec2 pos = gl_FragCoord.xy;
    pos -= resolution / 2.0;
    float x = pos.x - center.x;
    float y = pos.y - center.y;
    float col = abs(x * x + y * y - r * r) - r * strokeWidth;

    // smooth out border
    /* col /= resolution.x * resolution.y * 0.5; */
    /* col = sqrt(col); */

    // invert color
    col = 1.0 - col;

	gl_FragColor = vec4(vec3(col), 1.0);
}

