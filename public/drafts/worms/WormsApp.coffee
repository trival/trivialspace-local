### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "lib/app/BasicThreeJsApp"
  "lib/app/FirstPersonControls"
  "lib/utils/math/functions"
  "lib/utils/graphics/imageDataContext"
  "lib/utils/graphics/functions"
  "lib/utils/js/functions"
  "text!draft/code/tile.frag"
  "js!external/threejs_extras/controls/FlyControls"
  "js!external/threejs_extras/shaders/CopyShader"

],

(BasicApp, FirstPersonControls, math, pixelCtx, graphics, {extend}, fragmentShader) ->


  class Walker

    x: 0
    y: 0
    width: 0
    height: 0
    rad: 0
    dir: [0, 0]
    brush: null
    maxLight: 255
    currentColor: [0, 0, 0, 255]
    basicColor: [255, 255, 255, 1]

    constructor: (args) ->
      extend @, args
      @maxLight = Math.max @basicColor[0], @basicColor[1], @basicColor[2]

    adjustPosition: (newPos) ->
      newPos[0] += @width if newPos[0] < 0
      newPos[0] -= @width if newPos[0] >= @width
      newPos[1] += @height if newPos[1] < 0
      newPos[1] -= @height if newPos[1] >= @height
      newPos

    walk: ->
      chanceX = 0.6
      chanceY = 0.2
      velo = if Math.random() < 0.8 then 2 else 1
      xDir = math.sign Math.random() - chanceX
      yDir = math.sign Math.random() - chanceY
      xNew = @x + xDir * velo
      yNew = @y + yDir * velo
      @dir = [@x + xDir * @rad, @y + yDir * @rad]
      [@x, @y] = @adjustPosition [xNew, yNew]

    absorbColor: ([r, g, b, _], timer) ->
      a = Math.max 0, Math.max(r, g, b) - 32

      if a > 0
        a = 64
        @currentColor = graphics.mixColors @currentColor, [r, g, b, a]
        [rC, gC, bC, _] = @currentColor
        if @maxLight - 7 > Math.max rC, gC, bC
          rC += 7
          gC += 7
          bC += 7
          @currentColor = [rC, gC, bC, 255]

      else if timer == 0
        @currentColor = graphics.mixColors @currentColor, @basicColor
        # console.debug @basicColor

      @brush.mixinColor @currentColor


  class MetalPlatesApp extends BasicApp

    mesh: null
    controls: null
    walkers: []
    timer: 0

    canvas: (->
      canvas = document.createElement "canvas"
      canvas.width = 512
      canvas.height = 512
      ctx = canvas.getContext "2d"
      ctx.fillStyle = "black"
      ctx.fillRect 0, 0, canvas.width, canvas.height
      canvas
    )()


    createBrush : ->
      c = document.createElement "canvas"
      rad = 6 + math.randInt 4
      c.width = rad * 2
      c.height = rad * 2
      ctx = c.getContext "2d"
      ctx.fillStyle = "rgba(0,0,0,0)"
      ctx.fillRect 0, 0, rad * 2, rad * 2
      grd = ctx.createRadialGradient rad, rad, 1, rad, rad, rad
      grd.addColorStop 0, "rgba(255, 255, 255, 0.2)"
      grd.addColorStop 1, "rgba(255, 255, 255, 0.0)"
      ctx.fillStyle = grd
      ctx.fillRect 0, 0, rad * 2, rad * 2
      pixelCtx.getPixelContext ctx.getImageData 0, 0, rad * 2, rad * 2


    initWalkers: ->
      @walkers = for i in [0..95]
        do =>

          r = 55 + math.randInt 200
          g = 55 + math.randInt 200
          b = 55 + math.randInt 200

          # r = 0
          # g = 0
          # b = 128

          # if Math.random() > 0.5
            # g = 255
          # else
            # r = 255

          brush = @createBrush()
          brush.mixinColor [r, g, b, 255]
          rad = Math.floor brush.width() / 2

          new Walker
            x: math.randInt(@canvas.width),
            y: math.randInt(@canvas.height),
            width: @canvas.width,
            height: @canvas.height,
            brush: brush
            rad: rad
            basicColor: [r, g, b, 12]
            currentColor: [r, g, b, 255]


    init: ->
      @renderer.setClearColorHex 0x000000, 1
      @renderer.antialias = false

      @camera.position.z = 3.5
      @camera.lookAt new THREE.Vector3 0,0,0

      @controls = new FirstPersonControls @camera, @domElement
      @controls.flyMode = true
      @controls.lon = -90

      @texture = new THREE.Texture @canvas, THREE.UVMapping, THREE.RepeatWrapping, THREE.RepeatWrapping

      plane = new THREE.PlaneGeometry 4, 4, 30, 30
      material = new THREE.ShaderMaterial
        uniforms:
          texture:
            type: 't'
            value: @texture
          tileCount:
            type: 'f'
            value: 1.4
        vertexShader: THREE.CopyShader.vertexShader
        fragmentShader: fragmentShader

      @mesh = new THREE.Mesh plane, material
      @scene.add @mesh

      @initWalkers()


    draw: ->
      cCtx = @canvas.getContext "2d"
      imgData = cCtx.getImageData 0, 0, @canvas.width, @canvas.height
      ctx = pixelCtx.getRepeatedPixelContext imgData
      @timer += 1
      @timer = @timer % 8
      ctx.decreaseAllBy [1, 1, 1, 0] if @timer % 3

      for walker in @walkers
        walker.walk()
        walker.absorbColor ctx.getColorAt(walker.dir), @timer
        ctx.drawImageAt walker.brush, [walker.x - walker.rad, walker.y - walker.rad]

      cCtx.putImageData ctx.imageData, 0, 0


    update: ->
      @draw()
      @texture.needsUpdate = true
      # @mesh.rotation.x += 0.001
      # @mesh.rotation.y += 0.002
      @controls.update()


