uniform sampler2D randomTexture;
uniform sampler2D normalTexture;

varying vec2 vTexCoord;
varying vec3 vViewdir;
varying vec3 vTangent;
varying vec3 vBinormal;
varying vec3 vNormal;

const float uShininess = 30.0;
const vec2 PLATESIZE = vec2(0.15, 0.15);

void main() {
    vec2 position = vTexCoord / PLATESIZE;
    vec2 platePosition = fract(position);
    vec2 plate = floor(position);

	vec4 rand;
	vec3 normal = vec3(0.0, 0.0 ,1.0);
    vec3 color = vec3(1.0);
	bool topPlate = false;

    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j<=1; j++) {
            vec2 offset = vec2(float(i), float(j));
            vec2 currentCell = (plate + offset + vec2(1.0));
            rand = texture2D(randomTexture, currentCell * PLATESIZE * vec2(0.01) + vec2(0.1));

            vec2 currentScalePosition = (platePosition - offset - vec2(0.5))
                * (rand.rg * vec2(0.4) + vec2(0.6)) 
                + vec2(0.5);
            bvec2 zero = lessThan(currentScalePosition, vec2(0.0));
            bvec2 one = greaterThan(currentScalePosition, vec2(1.0));
            if (!(zero.x || zero.y) && !(one.x || one.y)) {
                vec3 normalTemp = texture2D(normalTexture, currentScalePosition).xyz;
                normalTemp = normalTemp * vec3(2.0) - vec3(1.0);
                normal = normalTemp + (rand.xyz - vec3(0.5)) * vec3(0.1);
				float isTop = step(0.65, rand.w);
                if (isTop == 1.0) {
                    topPlate = true;
                	break;
                }
            }
        }
        if (topPlate) {
         	break;
        }
    }
    
    
    normal = normalize(normal);
    mat3 tbnMat = mat3(vTangent,vBinormal,vNormal);
    normal = tbnMat * normal;

    vec3 matColor = vec3(0.5);
    // vec3 matColor = texture2D(randomTexture,vTexCoord).xyz;
    color = ambientLightColor  
        + calculatePointLighting(normal, vViewdir, 30.0, matColor, vec3(1.0))
        + calculateDirectionalLighting(normal, vViewdir, 30.0, matColor, vec3(1.0));
    gl_FragColor = vec4(color, 1.0);
}

