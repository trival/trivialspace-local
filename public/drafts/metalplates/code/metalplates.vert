attribute vec4 tangent;

varying vec2 vTexCoord;
varying vec3 vViewdir;
varying vec3 vTangent;
varying vec3 vBinormal;
varying vec3 vNormal;

void main() {
    vec4 pos = vec4(position,1.0);
    vec3 mvPos = (modelViewMatrix * pos).xyz;
    gl_Position = projectionMatrix * modelViewMatrix * pos;
    vTexCoord = uv;

    vNormal = normalize(normalMatrix * normal);
    vTangent = normalize(normalMatrix * tangent.xyz);
    vBinormal = cross( vNormal, vTangent ) * tangent.w;
    vBinormal = normalize(vBinormal);

    vViewdir = normalize(-mvPos);

    calculatePointLightVectors(mvPos);
}
