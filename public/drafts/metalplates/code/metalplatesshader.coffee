define [
  "text!./metalplates.vert"
  "text!./metalplates.frag"
  "text!assets/shaderlibs/lighting.vertlib"
  "text!assets/shaderlibs/lighting.fraglib"
], (vert, frag, libvert, libfrag) ->

  vertex: libvert + '\n' + vert
  fragment: libfrag + '\n' + frag
