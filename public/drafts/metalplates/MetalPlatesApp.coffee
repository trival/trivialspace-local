### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "lib/app/BasicThreeJsApp"
  "lib/app/FirstPersonControls"
  "lib/widgets/lightsController"
  "lib/utils/threejs/helpers"
  "draft/code/metalplatesshader"
  "text!draft/models/blender_cube.js"
  "text!assets/models/directioncube.js"

], (BasicApp, 
    FirstPersonControls, 
    lightsController, 
    {texture}
    shader, 
    model, 
    directioncube) ->


  class MetalPlatesApp extends BasicApp

    textureUrls: [
      PATH_TO_DRAFT + 'textures/plate_normal.jpg'
      '/commons/assets/textures/directioncubemap.png'
    ]

    mesh: null
    controls: null


    createRandomTex: ->
      canvas = document.createElement "canvas"
      ctx = canvas.getContext "2d"
      w = h = 512
      canvas.width = w
      canvas.height = h
      imgData = ctx.getImageData 0, 0, w, h
      data = imgData.data

      for i in [0..(w*h*4)]
        data[i] = Math.floor Math.random() * 256

      ctx.putImageData imgData, 0, 0

      # dataURL = canvas.toDataURL "image/png"
      # window.open dataURL

      texture canvas


    init: ->

      @renderer.setClearColorHex 0x000000, 1

      @camera.position.z = 3.5
      @camera.lookAt new THREE.Vector3 0,0,-1

      @controls = new FirstPersonControls @camera, @domElement
      @controls.flyMode = true
      @controls.lon = -90

      [normalTexture, directionTexture] = @textures
      randomTexture = @createRandomTex()

      uniforms = THREE.UniformsUtils.merge [THREE.UniformsLib.lights,
        normalTexture:
          type: "t"
          value: null

        randomTexture:
          type: "t"
          value: null ]

      uniforms.normalTexture.value = normalTexture
      uniforms.randomTexture.value = randomTexture

      geometry = @modelLoader.load model
      geometry.computeTangents()
      material = new THREE.ShaderMaterial
        fragmentShader: shader.fragment
        vertexShader: shader.vertex
        uniforms: uniforms
        lights: true

      @mesh = new THREE.Mesh geometry, material
      @scene.add @mesh

      ambientLight = new THREE.AmbientLight 0x050505
      @scene.add ambientLight
      pointLight1 = new THREE.PointLight 0xee44ee
      pointLight1.position.z = 8.0
      @scene.add pointLight1
      pointLight2 = new THREE.PointLight 0xeeff5
      pointLight2.position.x = 8.0
      @scene.add pointLight2
      directionalLight = new THREE.DirectionalLight 0xffee66
      directionalLight.position = new THREE.Vector3 1,1,-1
      @scene.add directionalLight

      # directionMaterial = new THREE.MeshLambertMaterial map: directionTexture
      # directionGeometry = @modelLoader.load directionCube
      # directionMesh = new THREE.Mesh directionGeometry, directionMaterial
      # directionMesh.scale.set 30,30,30
      # @scene.add directionMesh

      # lightsController.create @scene, 0.2

      center = new THREE.Vector3 0,0,0
      document.addEventListener "keypress", (evt) =>
        if ' '.charCodeAt(0) == evt.charCode
          @camera.lookAt center


    update: ->
      @mesh.rotation.x += 0.001
      @mesh.rotation.y += 0.002
      @controls.update()

