###
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "lib/app/BasicThreeJsApp"
  "lib/utils/math/functions"
  "lib/utils/math/vectors"
  "lib/utils/graphics/imageDataContext"
  "lib/utils/threejs/helpers"
  "lib/utils/math/noise"
  "text!draft/code/rain.frag"
  "text!draft/code/rainNormals.frag"
  "text!draft/code/rain.vert"

], (BasicApp
    {sign, randInt}
    {subScalar, add}
    pixelCtx
    {renderTarget}
    {noise1D}
    compositionShader, normalShader, vertexShader) ->


  console.debug subScalar

  RAIN_CANVAS_SIZE = 512
  RAIN_CHANCE = 0.2
  CHANCE_X_DIR = 0.7
  CHANCE_Y_DIR = 0.2
  MAX_AUTONOMOUS_COLOR = 190

  raindrops = []

  getNextDropPosition = ([x, y]) ->
    velo = if Math.random() < 0.8 then 2 else 1
    xDir = sign Math.random() - CHANCE_X_DIR
    yDir = sign Math.random() - CHANCE_Y_DIR
    xNew = x + xDir * velo
    yNew = y + yDir * velo * 2
    [x, y] = adjustPosition [xNew, yNew]


  adjustPosition = ([x, y]) ->
    x += RAIN_CANVAS_SIZE if x < 0
    x -= RAIN_CANVAS_SIZE if x >= RAIN_CANVAS_SIZE
    y += RAIN_CANVAS_SIZE if y < 0
    y -= RAIN_CANVAS_SIZE if y >= RAIN_CANVAS_SIZE
    [x, y]


  newBrush = (rad) ->
    c = document.createElement "canvas"
    c.width = rad * 2
    c.height = rad * 2
    ctx = c.getContext "2d"
    ctx.fillStyle = "rgba(0,0,0,0)"
    ctx.fillRect 0, 0, rad * 2, rad * 2
    grd = ctx.createRadialGradient rad, rad, 1, rad, rad, rad
    grd.addColorStop 0, "rgba(255, 0, 0, 0.25)"
    grd.addColorStop 1, "rgba(255, 0, 0, 0.0)"
    ctx.fillStyle = grd
    ctx.fillRect 0, 0, rad * 2, rad * 2
    pixelCtx.getPixelContext ctx.getImageData 0, 0, rad * 2, rad * 2


  newDrop = ->
    radius = 6 + randInt 4
    drop =
      pos: [randInt(RAIN_CANVAS_SIZE), randInt(RAIN_CANVAS_SIZE)]
      brush: newBrush radius
      rad: radius


  updateRain = (isAutonomous) ->
    raindrops = for drop in raindrops when isAutonomous drop
      drop.pos = getNextDropPosition drop.pos
      drop

    raindrops.push newDrop() if Math.random() < RAIN_CHANCE


  class BehindGlassApp extends BasicApp

    mesh: null
    controls: null
    raindrops: []
    trackMouse: false
    texIndex: 0
    noiseParam: 0

    textureUrls: (PATH_TO_DRAFT + "textures/landscape#{i}.jpg" for i in [0..2])

    rainMap: (->
      canvas = document.createElement "canvas"
      canvas.width = RAIN_CANVAS_SIZE
      canvas.height = RAIN_CANVAS_SIZE
      ctx = canvas.getContext "2d"
      ctx.fillStyle = "black"
      ctx.fillRect 0, 0, canvas.width, canvas.height
      canvas
    )()


    initKeyEvents: ->
      document.addEventListener "keypress", (evt) =>
        switch evt.charCode
          when 'n'.charCodeAt 0
            @texIndex = (@texIndex + 1) % 3
            @material.uniforms.viewMap.value = @textures[@texIndex]


    init: ->
      @renderer.setClearColorHex 0x000000, 1
      @renderer.antialias = false

      @camera = new THREE.Camera
      @camera.position.z = 1

      @rainTexture = new THREE.Texture @rainMap
      viewMap = @textures[2]

      plane = new THREE.PlaneGeometry 2, 2

      rainNormalsMaterial = new THREE.ShaderMaterial
        uniforms:
          tDiffuse:
            type: 't'
            value: @rainTexture
          resolution:
            type: 'v2'
            value: new THREE.Vector2 RAIN_CANVAS_SIZE, RAIN_CANVAS_SIZE
        vertexShader: vertexShader
        fragmentShader: normalShader
      @rainTexScene = new THREE.Scene
      @rainTexScene.add new THREE.Mesh plane, rainNormalsMaterial

      @rainTarget = renderTarget RAIN_CANVAS_SIZE, RAIN_CANVAS_SIZE
      @rainTarget.wrapS = @rainTarget.wrapT = THREE.RepeatWrapping

      @material = new THREE.ShaderMaterial
        uniforms:
          viewMap:
            type: 't'
            value: viewMap
          rainMap:
            type: 't'
            value: @rainTarget
          rainTileCount:
            type: 'f'
            value: 1.9
          resolution:
            type: 'v2'
            value: new THREE.Vector2 @width, @height
          mouseCoords:
            type: 'v2'
            value: new THREE.Vector2 @mouseX, @mouseY
        vertexShader: vertexShader
        fragmentShader: compositionShader

      @scene.add new THREE.Mesh plane, @material

      @initKeyEvents()

      raindrops = for i in [1..50]
        newDrop()


    draw: ->
      cCtx = @rainMap.getContext "2d"
      imgData = cCtx.getImageData 0, 0, @rainMap.width, @rainMap.height
      ctx = pixelCtx.getRepeatedPixelContext imgData

      ctx.decreaseAllBy [1, 0, 0, 0]

      isAutonomous = (drop) ->
        ctx.getColorAt(add drop.pos, [0, drop.rad])[0] < MAX_AUTONOMOUS_COLOR

      updateRain isAutonomous

      for drop in raindrops
        ctx.drawImageAt drop.brush, subScalar drop.pos, drop.rad

      cCtx.putImageData ctx.imageData, 0, 0


    update: ->
      @draw()
      @randomMouseCoords()
      @rainTexture.needsUpdate = true
      @material.uniforms.mouseCoords.value.x = @mouseX
      @material.uniforms.mouseCoords.value.y = @mouseY
      @renderer.render @rainTexScene, @camera, @rainTarget, true


    randomMouseCoords: ->
      @noiseParam += 0.004
      noiseYParam = @noiseParam + 2.76
      noiseX = noise1D(@noiseParam) * 0.5 +
        noise1D(@noiseParam * 2.0) * 0.27 +
        noise1D(@noiseParam * 4.0) * 0.14 +
        noise1D(@noiseParam * 8.0) * 0.06 +
        noise1D(@noiseParam * 8.0) * 0.07
      noiseY = noise1D(noiseYParam) * 0.5 +
        noise1D(noiseYParam * 2.0) * 0.27 +
        noise1D(noiseYParam * 4.0) * 0.14 +
        noise1D(noiseYParam * 8.0) * 0.06 +
        noise1D(noiseYParam * 8.0) * 0.03
      @mouseX = noiseX * 2.0
      @mouseY = noiseY * 1.5
