__Behind glass__ creates the illusion of space with the technique of movement parallax. The raindrops are created by a very simple implementation of [autonomous agents](http://en.wikipedia.org/wiki/Autonomous_agent) and [random walk](http://en.wikipedia.org/wiki/Random_walk) algorithms and are blended with the background with the help of some even simpler tricks. It is basically just texture coordinates manipulation according to a normalmap, that is calculated on the fly based on the movement of the raindrop agents.

Code, artwork and photographs by Thomas Gorny.

## Navigation

__Press 'n' button__ to change the scenery

## Planned updates

* Rearchitect the fluid effect to increase realism and aesthetics.
* Include more suitable backgrounds.

## Changelog

__version 0.1__ (2013-04-07): 

* Initial release
