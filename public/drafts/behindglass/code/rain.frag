uniform sampler2D rainMap;
uniform sampler2D viewMap;
uniform float rainTileCount;
uniform vec2 resolution;
uniform vec2 mouseCoords;
uniform float viewRatio;

void main() {
    vec2 uvView = gl_FragCoord.xy / resolution; 
    vec2 uvRain = gl_FragCoord.xy / resolution.y; 
    uvRain *= vec2(rainTileCount);
    vec2 mouseOffset = mouseCoords * 0.5;
    mouseOffset.y *= -1.0;
    vec2 mouseOffsetRain = mouseOffset * 0.2;
    uvRain *= 0.8;
    uvRain += mouseOffsetRain;
    vec2 mouseOffsetView = mouseOffset * 0.02;
    uvView = uvView * 0.98 + 0.01;
    uvView += mouseOffsetView;
    vec4 rain = texture2D( rainMap, uvRain, 0.2 );
    vec4 rainColor = vec4(vec3(rain.r), 1.0);
    vec2 fresnel = (rain.gb - 0.5) / 3.0;
    uvView -= fresnel;
    vec4 view = texture2D( viewMap, uvView);
    gl_FragColor = view + rainColor / 19.0;
    /* gl_FragColor = vec4(0.5, rain.g, rain.b, 1.0); */
    /* gl_FragColor = rain; */
}
