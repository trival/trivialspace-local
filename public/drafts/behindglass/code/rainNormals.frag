uniform sampler2D tDiffuse;
uniform vec2 resolution;

void main() {
    vec2 uv = gl_FragCoord.xy/resolution; 
    vec2 uvPrev = uv - vec2(1.0)/resolution;
    float r = texture2D(tDiffuse, uv).r;
    float rPrevX = texture2D(tDiffuse, vec2(uvPrev.x, uv.y)).r;
    float rPrevY = texture2D(tDiffuse, vec2(uv.x, uvPrev.y)).r;
    float g = (rPrevX - r) / 2.0 + 0.5;
    float b = (rPrevY - r) / 2.0 + 0.5;
    gl_FragColor = vec4(r, g, b, 1.0);
    /* gl_FragColor = vec4(vec3(r), 1.0); */
}
