 # @author alteredq / http://alteredqualia.com/

define [
  "text!assets/shaderlibs/PhotoshopMathFP.glsl"
  "text!./composeBloom.frag"
  "lib/utils/threejs/helpers"
  'js!external/threejs_extras/shaders/ConvolutionShader'
  'js!external/threejs_extras/shaders/CopyShader'
], (colorEffects, composeShader
    {renderTarget}) ->

  _blurX = new THREE.Vector2( 0.001953125, 0.0 )
  _blurY = new THREE.Vector2( 0.0, 0.001953125 )
  fragmentShader = colorEffects + "\n" + composeShader

  BlurMapPass = ( width, height, blurmap, strength) ->

    strength ?= 1

    # render targets

    @renderTargetX = renderTarget width, height
    @renderTargetY = renderTarget width, height

    # copy material

    copyShader = THREE.CopyShader

    @copyUniforms = THREE.UniformsUtils.clone copyShader.uniforms

    @copyUniforms[ "opacity" ].value = strength
    @copyUniforms[ "original"] =
      type: 't'
      value: null

    @materialCopy = new THREE.ShaderMaterial
      uniforms: @copyUniforms,
      vertexShader: copyShader.vertexShader,
      fragmentShader: fragmentShader,

    # convolution material

    convolutionShader = THREE.ConvolutionShader

    @convolutionUniforms = THREE.UniformsUtils.clone convolutionShader.uniforms

    @convolutionUniforms[ "uImageIncrement" ].value = _blurX
    @convolutionUniforms[ "cKernel" ].value = THREE.ConvolutionShader.buildKernel sigma

    @materialConvolution = new THREE.ShaderMaterial
      uniforms: @convolutionUniforms,
      vertexShader:  convolutionShader.vertexShader,
      fragmentShader: convolutionShader.fragmentShader,
      defines:
        "KERNEL_SIZE_FLOAT": kernelSize.toFixed( 1 ),
        "KERNEL_SIZE_INT": kernelSize.toFixed( 0 )

    @enabled = true
    @needsSwap = false
    @clear = false


  BloomPass.prototype =

    render: ( renderer, writeBuffer, readBuffer, delta, maskActive ) ->

      if maskActive
        renderer.context.disable renderer.context.STENCIL_TEST

      # Render quad with blured scene into texture (convolution pass 1)

      THREE.EffectComposer.quad.material = @materialConvolution

      @convolutionUniforms[ "tDiffuse" ].value = readBuffer
      @convolutionUniforms[ "uImageIncrement" ].value = _blurX

      renderer.render THREE.EffectComposer.scene, THREE.EffectComposer.camera, @renderTargetX, true


      # Render quad with blured scene into texture (convolution pass 2)

      @convolutionUniforms[ "tDiffuse" ].value = @renderTargetX
      @convolutionUniforms[ "uImageIncrement" ].value = _blurY

      renderer.render THREE.EffectComposer.scene, THREE.EffectComposer.camera, @renderTargetY, true
      # renderer.render THREE.EffectComposer.scene, THREE.EffectComposer.camera, readBuffer, @clear

      # Render original scene with superimposed blur to texture

      THREE.EffectComposer.quad.material = @materialCopy

      @copyUniforms[ "tDiffuse" ].value = @renderTargetY
      @copyUniforms[ "original"].value = readBuffer

      if maskActive
        renderer.context.enable renderer.context.STENCIL_TEST

      renderer.render THREE.EffectComposer.scene, THREE.EffectComposer.camera, readBuffer, @clear

    resize: (width, height) ->

      @renderTargetX.width = width
      @renderTargetX.height = height
      @renderTargetY.width = width
      @renderTargetY.height = height


  BloomPass

