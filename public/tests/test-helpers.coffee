define

  testInputs: (func, args, results) ->
    for params, i in args
      assert.equals func(params...), results[i]
    null
