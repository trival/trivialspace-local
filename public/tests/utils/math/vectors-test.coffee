define [
  "lib/utils/math/vectors"
  "tests/test-helpers"
], ( {add, sub, div, length, normalize},
     {testInputs} ) ->

  buster.testCase "vector functions",

    "add function": ->
      testInputs add,
        [ [[5, 1, 6], [1, 2, 3]]
          [[5, 1, 4, 7], [2, 3, 4, 5]]
          [[-1, 1], [3, -4]] ],

        [ [6, 3, 9],
          [7, 4, 8, 12],
          [2, -3] ]

    "sub function": ->
      testInputs sub,
        [ [[5, 1, 6], [1, 2, 3]]
          [[5, 1, 4, 7], [2, 3, 4, 5]]
          [[-1, 1], [3, -4]] ],

        [ [4, -1, 3],
          [3, -2, 0, 2],
          [-4, 5] ]

    "div function": ->
      testInputs div,
        [ [[4, 6], 2]
          [[1, 3], 0] ],

        [ [2, 3],
          undefined ]

    "length function": ->
      testInputs length,
        [ [[3,4]]
          [[2,4,4]] ],

        [5, 6]

    "normalize function": ->
      assert 0.9999 < length(normalize [2, 3, 4]) < 1.0001
      assert 0.9999 < length(normalize [-1, 3]) < 1.0001
      assert 0.9999 < length(normalize [2, 3, 4, 5]) < 1.0001
