define [
  "lib/utils/math/functions"
  "tests/test-helpers"
], (math, helpers) ->

  buster.testCase "math functions",

    "clamp function": ->
      helpers.testInputs math.clamp,
        [ [5, 1, 6]
          [5, 1, 4]
          [-1, 1, 6]
          [7, 1, 6] ],

        [5, 4, 1, 6]
