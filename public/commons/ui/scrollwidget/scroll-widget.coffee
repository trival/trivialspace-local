### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "jquery"
  "underscore"
  "lib/utils/math/functions"

  "external/jquery.mousewheel"
  "external/requestAnimationFrame"
  "css!./scrollwidget.css"

], ($,
    {partial, clone},
    {clamp}) ->


  $.fn.scrollPane = ->

    return this.each ->

      scrolling = false
      dragging = false
      pointerY = 0
      noClick = false
      state = null
      element = this

      $element = $ this
      $upButton = $ "<div class='scroll-up-button'><i class='icon-chevron-up'></i></div>"
      $downButton = $ "<div class='scroll-down-button'><i class='icon-chevron-down'></i></div>"
      $element.append($upButton).append $downButton


      updateState = ->
        scrollLength = element.scrollHeight - element.clientHeight
        state =
          isAllVisible: scrollLength <= 2
          isAtTop: element.scrollTop <= 1
          isAtBottom: element.scrollTop >= scrollLength - 1
          scrollLength: scrollLength
          scrollUp: element.scrollTop
          scrollDown: scrollLength - element.scrollTop
          clientHeight: element.clientHeight


      adjustButtonPos = ->
        $upButton.css 'top', element.scrollTop
        $downButton.css 'bottom', -element.scrollTop


      onStateChange = ->
        # bugfix for resize misbehavior of upButton
        oldClientHeight = if state then state.clientHeight else element.clientHeight
        deltaHeight = element.clientHeight - oldClientHeight
        element.scrollTop -= deltaHeight if deltaHeight > 0

        {isAllVisible, isAtTop, isAtBottom, scrollLength} = updateState()
        adjustButtonPos()

        # $downButton.finish() # resizing down bugfix
        if isAllVisible
          $upButton.fadeOut 'slow'
          $downButton.fadeOut 'slow'
        else
          if isAtTop then $upButton.fadeOut "slow" else $upButton.fadeIn "fast"
          if isAtBottom then $downButton.fadeOut "slow" else $downButton.fadeIn "fast"


      buttonScroll = (amount) ->
        if scrolling
          element.scrollTop += amount
          adjustButtonPos()
          requestAnimationFrame partial buttonScroll, amount


      dragScroll = (evt) ->
        {isAllVisible, scrollLength, scrollUp, scrollDown} = state

        if dragging and not isAllVisible
          evt.preventDefault()
          scrollPos = evt.screenY - pointerY
          scrollPos = clamp scrollPos, -scrollDown, scrollUp
          noClick = 10 < Math.abs scrollPos
          element.scrollTop = scrollUp - scrollPos
          adjustButtonPos()


      wheelScroll = (event, delta) ->
        element.scrollTop -= delta * 10
        adjustButtonPos()
        onStateChange()


      scrollEnd = ->
        onStateChange() if scrolling or dragging
        dragging = false
        scrolling = false


      $element.css 'position', 'relative' if $element.css('position') is 'static'

      $element.on 'mousemove', dragScroll

      $element.on 'mousedown', (evt) ->
        noClick = false
        unless state.isAllVisible
          pointerY = evt.screenY
          dragging = true

      $element.mousewheel wheelScroll

      $element.on 'mousemove, mousedown', (evt) ->
        state.isAllVisible

      $element.on 'click', 'a', ->
        noClick = false if noClick
        
      $upButton.on 'mousedown', (e) ->
        scrolling = true
        buttonScroll -7
        false

      $downButton.on 'mousedown', (e) ->
        scrolling = true
        buttonScroll 7
        false

      $(document).on 'mouseup.scrollpane', scrollEnd

      $element.on 'scrollstatechange.scrollpane', onStateChange

      onStateChange()
