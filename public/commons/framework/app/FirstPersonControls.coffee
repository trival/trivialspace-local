### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define ->

  class FirstPersonControls

    movementSpeed: 1.0
    lookSpeed: 4
    activeLook: true
    flyMode: false


    constructor: (@camera, domElement) ->
      domElement ?= document

      @moveTarget = new THREE.Vector3(0, 0, 0)
      @lookTarget = new THREE.Vector3(0, 0, 0)
      @lastUpdate = new Date().getTime()
      @tdiff = 0
      @mouseDeltaX = 0
      @mouseDeltaY = 0
      @mouseX = 0
      @mouseY = 0
      @moveForward = false
      @moveBackward = false
      @moveLeft = false
      @moveRight = false
      @lat = 0
      @lon = 0
      @phi = 0
      @theta = 0
      @mouseDragOn = false

      domElement.addEventListener "mousedown", @onMouseDown
      domElement.addEventListener "contextmenu", (event) ->
        event.preventDefault()

      document.addEventListener "mousemove", @onMouseMove
      document.addEventListener "mouseup", @onMouseUp
      document.addEventListener "keydown", @onKeyDown
      document.addEventListener "keyup", @onKeyUp


    onMouseDown: (event) =>
      event.preventDefault()
      event.stopPropagation()
      if @activeLook
        switch event.button
          when 0
            @mouseX = event.screenX
            @mouseY = event.screenY
            @mouseDragOn = true
          when 1
            @moveBackward = true
          when 2
            @moveForward = true


    onMouseUp: (event) =>
      event.preventDefault()
      event.stopPropagation()
      if @activeLook
        switch event.button
          when 0
            @mouseDragOn = false
            @mouseDeltaX = 0
            @mouseDeltaY = 0
          when 1
            @moveBackward = false
          when 2
            @moveForward = false


    onMouseMove: (event) =>
      if @mouseDragOn
        @mouseDeltaX = event.screenX - @mouseX
        @mouseDeltaY = event.screenY - @mouseY
        @mouseX = event.screenX
        @mouseY = event.screenY


    onKeyDown: (event) =>
      switch event.keyCode
        # arrow keys need to disable scroll
        when 38
          @moveForward = true
          event.preventDefault()
        when 37
          @moveLeft = true
          event.preventDefault()
        when 40
          @moveBackward = true
          event.preventDefault()
        when 39
          @moveRight = true
          event.preventDefault()
        when 87
          @moveForward = true
        when 65
          @moveLeft = true
        when 83
          @moveBackward = true
        when 68
          @moveRight = true
        when 82
          @moveUp = true  if @flyMode
        when 70
          @moveDown = true  if @flyMode


    onKeyUp: (event) =>
      switch event.keyCode
        when 38, 87
          @moveForward = false
        when 37, 65
          @moveLeft = false
        when 40, 83
          @moveBackward = false
        when 39, 68
          @moveRight = false
        when 82
          @moveUp = false
        when 70
          @moveDown = false


    update: ->
      now = new Date().getTime()
      @tdiff = (now - @lastUpdate) / 1000
      @lastUpdate = now

      @moveTarget.set 0, 0, 0
      @moveTarget.z = -1  if @moveForward
      @moveTarget.z = 1  if @moveBackward
      @moveTarget.x = -1  if @moveLeft
      @moveTarget.x = 1  if @moveRight
      @moveTarget.y = 1  if @moveUp
      @moveTarget.y = -1  if @moveDown

      unless @moveTarget.x is 0 and @moveTarget.y is 0 and @moveTarget.z is 0
        @moveTarget.transformDirection @camera.matrix
        @moveTarget.multiplyScalar @tdiff * @movementSpeed
        @manipulateMoveTarget @moveTarget
        @camera.position.add @moveTarget

      if @mouseDragOn
        actualLookSpeed = @tdiff * @lookSpeed
        actualLookSpeed = 0  unless @activeLook
        @lon += @mouseDeltaX * actualLookSpeed
        @lat -= @mouseDeltaY * actualLookSpeed
        @lat = Math.max(-85, Math.min(85, @lat))
        @phi = (90 - @lat) * Math.PI / 180
        @theta = @lon * Math.PI / 180
        position = @camera.position
        @lookTarget.x = position.x + 100 * Math.sin(@phi) * Math.cos(@theta)
        @lookTarget.y = position.y + 100 * Math.cos(@phi)
        @lookTarget.z = position.z + 100 * Math.sin(@phi) * Math.sin(@theta)
        @camera.lookAt @lookTarget
        @mouseDeltaX = 0
        @mouseDeltaY = 0


    manipulateMoveTarget: (targetVector) ->
