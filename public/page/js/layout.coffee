### 
Copyright (c) 2013 Thomas Gorny
The MIT License (see MIT-LICENSE.txt)
###

define [

  "jquery"
  "underscore"

  "ui/scrollwidget/scroll-widget"

], ($, 
    {partial}) ->

  $.noConflict()


  showingMenu = true

  $header = null
  $nav = null

  $menuButton = null
  $pauseButton = null
  $fullscreenButton = null

  mainElements = []

  hideTimer = 0


  initResponsiveElements = ->
    $header = $ 'header'
    $nav = $ '#draft-menu'
    $menuButton = $ '#menu-button'
    $pauseButton = $ '#pause-button'
    $fullscreenButton = $ '#fullscreen-button'
    $licenseButton = $ '#license-button'

    mainElements = [$header, $nav, $menuButton]


  showMenu = ->
    showingMenu = true
    $header.css 'display': "block"
    setTimeout (->
      element.removeClass("disabled") for element in mainElements
    ),10


  hideMenu = ->
    element.addClass("disabled") for element in mainElements
    showingMenu = false


  hideMenuTimeout = ->
    if showingMenu
      clearTimeout hideTimer
      hideTimer = setTimeout hideMenu, 1500
      

  arrangeContentHeight = ->
    winHeight = $(window).height()
    headerHeight = $header.outerHeight()

    $nav.css 'height', winHeight - headerHeight

    $nav.trigger 'scrollstatechange'
    setTimeout (-> $nav.trigger 'scrollstatechange'), 300


  # ------------------------- initialize layout -----------------------------

  # init all important clickevents
  initButtonActions = ->
    $menuButton.click ->
      if showingMenu
        hideMenu()
      else
        showMenu()


    $fullscreenButton.click ->
      elem = document.getElementById "draft-frame"
      if elem.requestFullscreen
        elem.requestFullscreen()
      else if elem.mozRequestFullScreen
        elem.mozRequestFullScreen()
      else if (elem.webkitRequestFullscreen)
        elem.webkitRequestFullscreen()


  # Basic interaction events
  initEvents = ->

    $header.on "transitionend webkitTransitionEnd", (evt) ->
      if evt.target is this and not showingMenu
        $header.css 'display', 'none'

    $('#draft-frame').on 'mouseenter', hideMenuTimeout

    element.on 'mouseenter mousemove', (-> clearTimeout hideTimer) for element in mainElements
    element.on 'mouseenter', showMenu for element in mainElements


  # Bind events on the running draft
  bindEventsOnDraft = (draft) ->
    $pauseButton.click ->
      draft.togglePause()

    draft.addEventListener 'pauseStateChanged', (evt) ->
      if evt.paused
        $pauseButton.find('i').removeClass('icon-pause').addClass 'icon-play'
        $pauseButton.find('.tooltip').html 'play'
      else
        $pauseButton.find('i').removeClass('icon-play').addClass 'icon-pause'
        $pauseButton.find('.tooltip').html 'pause'

    draft.addEventListener 'draftLoaded', ->
      hideMenuTimeout()
      $('#status-message').fadeOut 'fast'
      setTimeout (-> element.off 'mousemove' for element in mainElements), 1000



  #start initialization
  init = (ready) ->
    $ =>
      
      initResponsiveElements()
      initButtonActions()
      initEvents()

      # set initial layout
      arrangeContentHeight()
      setTimeout arrangeContentHeight, 5
      $(window).on 'resize', arrangeContentHeight
      $nav.scrollPane()

      # call the ready callback if it exists and init Events on running draft
      ready?()


  # --------------------------- public interface ---------------------------

  exports =
    # layout initialization public interface
    init: init

    bindEventsOnDraft: bindEventsOnDraft
